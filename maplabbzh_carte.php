<?PHP

	/*	#MapLabBZH La carte des FabLabs & Tiers Lieux de dissémination des usages numériques en Bretagne
		=================================================================================================
			Auteur	Guillaume Rouan | @grouan
			Blog	https://grouan.fr
		-------------------------------------------------------------------------------------------------
			Titre	= maplabbzh_carte.php
			Sources = https://gitlab.com/grouan/maplabbzh
			Licence	= CC BY-SA pour les contenus / EUPL pour le code source
			UI Liste = CSS Framework Bulma + Font Awesome
			UI Cartographie = Mapbox + OpenStreetMap
			Datas GeoJSON = Guillaume Rouan
			Création : octobre 2015 | v.4 juillet 2022
		-------------------------------------------------------------------------------------------------
	*/
	// Ouverture du fichier GEOjson
	//$fichier = 'adresses_fablab_bzh.geojson';
	$fichier = 'https://gitlab.com/grouan/maplabbzh/raw/master/map_bzh_fablab.geojson';
	$fp = fopen ($fichier, "r");
    	$contenu_du_fichier = stream_get_contents($fp); // en cas de fichier source local, utiliser : fread ($fp, filesize($fichier));
	fclose ($fp);

	// Décodage
	$json = json_decode ($contenu_du_fichier, true);

	// Récupération du style de carte
	if (isset($_GET['swz'])) { $_scrollWheelZoom = $_GET['swz']; } else { $_scrollWheelZoom = 'false'; }

	// Récupération du point de géoloc
	if (isset($_GET['type'])) { // Mini-carte d'un Pôle
		$_structure 	= $_GET['type']; // structure
		$_latitude 		= $_GET['lat']; $_longitude 	= $_GET['lon'];
		$_coordonnees	= $_latitude.', '.$_longitude;
		$_zoom				= 16;
		$_touchZoom		= 'false';
		$_dragging		= 'false';
	} else { // Carte globale de la Bretagne
		$_latitude = '48.05'; $_longitude = '-3.00';
		$_coordonnees	= $_latitude.', '.$_longitude; // LAT, LON
		$_zoom				= 7.6;
		$_touchZoom		= 'true';
		$_dragging		= 'true';
	}

	// Tableau des Lieux
	$tab_lieux = array('hakerspace','makerspace','tierslieu','usages','frenchtech','fablabMIT','fablab','cantine','formation');

?>
<!DOCTYPE html>
<html lang="fr">
<head>

		<!--

	       =============================================================================
	       	CARTE DES FABLABS ET TIERS-LIEUX DE BRETAGNE
	       =============================================================================
	       	Auteur : Guillaume ROUAN | https://grouan.fr | @grouan
	       -----------------------------------------------------------------------------
							 Titre : fablabs_bretagne.php
							 Sources : https://grouan.github.io/bzh_fablab/
							 Licence : CC-BY
							 UI :
							 	> Liste = CSS Framework Bulma + Font Awesome
							 	> Cartographie = Mapbox + OpenStreetMap
							 Datas des lieux : Guillaume Rouan
							 Création : octobre 2015 | v.4 juillet 2022
	             Thématiques : FabLab, MakerSpace, HackerSpace, FrenchTech,
	              		Cantine numérique, Tiers Lieux, Usages numériques,
	              		Formation / Recherche
	       -----------------------------------------------------------------------------

	       -->

		<title>Carte des Fablabs et Tiers-Lieux de dissémination des usages numériques en Bretagne #MapLabBzh - Guillaume Rouan - 2015 - CC BY</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link href="https://api.mapbox.com/mapbox-gl-js/v2.9.1/mapbox-gl.css" rel="stylesheet">
		<script src="https://api.mapbox.com/mapbox-gl-js/v2.9.1/mapbox-gl.js"></script>

<style>
	#map { position: absolute; top: 0; bottom: 0; width: 100%; }

/* === PARAMETRAGE DES MARKERS === */
<?PHP if (isset($_GET['type'])) { // Mini-carte d'un Pôle ?>
<?PHP echo '#'.$_structure.'_Icon'; ?> {
	background-image: url('https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_<?PHP echo $_structure; ?>.png');
	background-size: cover;
	width: 35px; height: 35px;
	cursor: pointer;
}
<?PHP } else { // Carte globale de la Bretagne
	/* style spécifique */
	$max = count($tab_lieux);
	for($i=0; $i < $max; $i++) { ?>
		<?PHP $temp_global_Icons .= '#'.$tab_lieux[$i].'_Icon';
		if ($i == ($max-1)) {  } else { $temp_global_Icons .= ', '; }
echo '#'.$tab_lieux[$i].'_Icon'; ?> { background-image: url('https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_<?PHP echo $tab_lieux[$i]; ?>.png'); }
<?PHP	} // for
	/* styles communs */
echo $temp_global_Icons; ?> {
	background-size: cover;
	width: 35px; height: 35px;
	cursor: pointer;
	}
<?PHP } // else ?>
	.mapboxgl-popup { max-width: 200px; }
</style>

<?php
	// STYLES : id => URL
	$tab_mapbox_styles = array (
		'streets' => 'mapbox://styles/mapbox/streets-v11', // Mapbox
		'light' => 'mapbox://styles/mapbox/light-v10',
		'dark' => 'mapbox://styles/mapbox/dark-v10',
		'outdoors' => 'mapbox://styles/mapbox/outdoors-v11',
		'satellite' => 'mapbox://styles/mapbox/satellite-v9',
		'satellite-streets' => 'mapbox://styles/mapbox/satellite-streets-v11',
		'navigation-day' => 'mapbox://styles/mapbox/navigation-day-v1',
		'navigation-night' => 'mapbox://styles/mapbox/navigation-night-v1',
	);
	if (isset($_GET['mapbox'])) { $_mapbox = $_GET['mapbox']; }
	else { $_mapbox = 'streets'; }
?>

</head>
<body style="margin:0px;padding:0px;">

<div id="map"></div>

<script>

	mapboxgl.accessToken = 'VOTRE-JETON-MAPBOX'; /* https://mapbox.com pour créer votre propre jeton */
  const map = new mapboxgl.Map({
		container: 'map', // container ID
		style: '<? echo $tab_mapbox_styles[$_mapbox]; ?>', // style URL
		center: [<?PHP echo $_longitude.', '.$_latitude; ?>],
		zoom: <?PHP echo $_zoom; ?>,
		projection: 'mercator', // albers, equalEarth, equirectangular, globe, lambertConformalConic, naturalEarth, winkelTripel
		keyboard: false,
		language: 'fr',
		logoPosition: 'bottom-right',
		zoomControl: true,
		doubleClickZoom: true,
		touchZoom: <?PHP echo $_touchZoom; ?>,
		dragging: <?PHP echo $_dragging; ?>,
		scrollZoom: <? echo $_scrollWheelZoom; ?>,
		hash: true,
		attributionControl: false
	})
	.addControl(new mapboxgl.AttributionControl({
		customAttribution: '<a href="https://grouan.fr" target="_blank" alt="Carte des FabLabs et Tiers Lieux de Bretagne - Guillaume Rouan">Guillaume Rouan</a> &middot; 2015 &middot; <a href="https://creativecommons.fr/licences/" target="_blank">CC BY</a> '
	}));

<?PHP
/* === AFFICHAGE DES MARQUEURS GEOLOCALISÉS === */

	$limit = 0;
	foreach ($json['features'] as $data) {

		// Paramétrage
		$longitude = $data['geometry']['coordinates'][0];
		$latitude = $data['geometry']['coordinates'][1];
		if ($data['properties']['web'] != "") { $web = '<a href="'.$data['properties']['web'].'" target="_blank"><img src="https://gitlab.com/grouan/maplabbzh/raw/master/img/ico_30x30px_WEB.png" title="Visitez '.$data['properties']['web'].'" style="border-radius:15px;margin:3px;" /></a>'; } else { $web = ''; }
		if ($data['properties']['facebook'] != "") { $facebook = '<a href="https://facebook.com/'.$data['properties']['facebook'].'" target="_blank"><img src="https://gitlab.com/grouan/maplabbzh/raw/master/img/ico_30x30px_FB.png" title="Suivez '.$data['properties']['facebook'].'" style="border-radius:15px;margin:3px;" /></a>'; } else { $facebook = ''; }
		if ($data['properties']['twitter'] != "") { $twitter = '<a href="https://twitter.com/'.$data['properties']['twitter'].'" target="_blank"><img src="https://gitlab.com/grouan/maplabbzh/raw/master/img/ico_30x30px_TW.png" title="Suivez '.$data['properties']['twitter'].'" style="border-radius:15px;margin:3px;" /></a>'; } else { $twitter = ''; }

		// Construction des marqueurs
echo "const popup_".$data['properties']['id']." = new mapboxgl.Popup({ offset: 25 }).setHTML('<b style=\"font-size:1.2em;text-transform:uppercase;\">".addslashes($data['properties']['name'])."</b><br />".addslashes($data['properties']['adresse'])."<br />".$data['properties']['cp']." ".addslashes($data['properties']['ville'])."<br />".$web." ".$facebook." ".$twitter."');\n";
echo "const el_".$data['properties']['id']." = document.createElement('div'); el_".$data['properties']['id'].".id = '".$data['properties']['structure']."_Icon';\n";
echo "new mapboxgl.Marker(el_".$data['properties']['id'].").setLngLat([".$longitude.','.$latitude."]).setPopup(popup_".$data['properties']['id'].").addTo(map);\n\n";

//if (++$limit == 10) break;

} ?>

</script>

</body>
</html>
