<!-- Footer -->
<section class="hero is-dark">
<div class="hero-body">
  <div class="container">
    <div class="content has-text-centered">
      <h2 style="color:#FFF;font-weight:bold;">#MapLabBZH</h2>
      <p><a href="https://grouan.fr" target="_blank">Guillaume Rouan</a> &middot; 2015 &middot; <a href="https://creativecommons.org/licenses/by-sa/2.0/fr/" target="_blank">CC BY-SA</a> + <a href="https://www.eupl.eu" target="_blank">EUPL</a></p>
  </div>
  </div>
</div>
</section>
