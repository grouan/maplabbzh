<?PHP

	/*	#MapLabBZH La carte des FabLabs & Tiers Lieux de dissémination des usages numériques en Bretagne
	=================================================================================================
		Auteur	Guillaume Rouan | @grouan
		Blog	https://grouan.fr
	-------------------------------------------------------------------------------------------------
		Titre	= maplabbzh_datalab.php
		Sources = https://gitlab.com/grouan/maplabbzh
		Licence	= CC BY-SA pour les contenus / EUPL pour le code source
		UI Liste = CSS Framework Bulma + Font Awesome
		UI Cartographie = Mapbox + OpenStreetMap
		Datas GeoJSON = Guillaume Rouan
		Création : octobre 2015 | v.4 juillet 2022
	-------------------------------------------------------------------------------------------------
*/


	// Package de fonctions
          require_once ('maplabbzh_fonctions.inc.php');

	// Sources JSON : Github
	$json_source = file_get_contents('https://raw.githubusercontent.com/grouan/bzh_fablab/master/map_bzh_fablab.geojson');

	//$json_data = json_decode($json_source); // Fichier brut
	$json_data = json_decode($json_source, true);  // Dans un tableau

	// export csv
	$export = 'id;nom;type;adresse;cp;ville;site_web;flux_rss;twitter;facebook;courriel;telephone;longitude;latitude'."\n";

	// Couleurs
	$t_couleurs = array (	'fablabMIT'	=> '#006699',	// bleu foncé
				'fablab' 		=> '#0099cc',	// bleu
				'makerspace'	=> '#66ccff',	// bleu clair
				'hakerspace'	=> '#6e2585',	// violet
				'tierslieu'	=> '#ffcc00',	// orange clair
				'usages'		=> '#ff6600',	// orange foncé
				'formation'	=> '#99cc00',	// vert
				'cantine' 	=> '#ff3399',	// rose
				'frenchtech'	=> '#cc0066'	// rose foncé
			);
	// Départements
	$t_dpt = array (
			'22' => 'Côtes-d\'Armor',
			'29' => 'Finistère',
			'35' => 'Ille-et-Vilaine',
			'44' => 'Pays-de-la-Loire',
			'56' => 'Morbihan'
		);
/*
array(2) {
  ["type"]=>string(17) "FeatureCollection"
  ["features"]=>array(88) {
    [0]=>array(3) {
      ["type"]=>string(7) "Feature"
      ["properties"]=>array(12) {
        ["id"]=>string(1) "1"
        ["name"]=>string(12) "MakerSpace56"
        ["structure"]=>string(9) "fablabMIT"
        ["adresse"]=>string(4) "VIPE"
        ["cp"]=>string(5) "56000"
        ["ville"]=>string(6) "Vannes"
        ["web"]=>string(23) "http://makerspace56.org"
        ["twitter"]=>string(13) "@makerspace56"
        ["facebook"]=>string(0) ""
        ["email"]=>string(24) "contact@makerspace56.org"
        ["tel"]=>string(0) ""
        ["rss"]=>string(29) "http://makerspace56.org/feed/"
      }
      ["geometry"]=>array(2) {
        ["type"]=>string(5) "Point"
        ["coordinates"]=>array(2) {
          [0]=>float(-2.7541303)
          [1]=>float(47.6424637)
        }
      }
    }
*/

	// Recherche de l'identifiant dans le fichier JSON
	$nb_references = count ($json_data['features']);

	$j=0; $k=0; $l=0; $m=0; $n=0; $o=0; $p=0; $q=0; $r=0;
	$cpt_22=0; $cpt_29=0; $cpt_35=0; $cpt_44=0; $cpt_56=0;
	$cpt_web=0; $cpt_rss=0; $cpt_facebook=0; $cpt_twitter=0;
	for ($i=0; $i < $nb_references; $i++) {

	// --- LISTE DES PÔLES ---

	    $tab_poles[$i]['id']		= $json_data['features'][$i]['properties']['id'];
	    $tab_poles[$i]['name']		= $json_data['features'][$i]['properties']['name'];
	    $tab_poles[$i]['structure'] 	= $json_data['features'][$i]['properties']['structure'];
	    $tab_poles[$i]['adresse'] 	= $json_data['features'][$i]['properties']['adresse'];
	    $tab_poles[$i]['cp'] 		= $json_data['features'][$i]['properties']['cp'];
	    $tab_poles[$i]['ville'] 		= $json_data['features'][$i]['properties']['ville'];
	    $tab_poles[$i]['web'] 		= $json_data['features'][$i]['properties']['web'];
	    $tab_poles[$i]['twitter'] 	= $json_data['features'][$i]['properties']['twitter'];
	    $tab_poles[$i]['facebook'] 	= $json_data['features'][$i]['properties']['facebook'];
	    $tab_poles[$i]['email'] 		= $json_data['features'][$i]['properties']['email'];
	    $tab_poles[$i]['tel'] 		= $json_data['features'][$i]['properties']['tel'];
	    $tab_poles[$i]['rss'] 		= $json_data['features'][$i]['properties']['rss'];
	    $tab_poles[$i]['longitude'] 	= $json_data['features'][$i]['geometry']['coordinates'][0];
	    $tab_poles[$i]['latitude'] 	= $json_data['features'][$i]['geometry']['coordinates'][1];

	// --- KPI ---

	    // => Volume pour chaque type de Pôle
	    if ($json_data['features'][$i]['properties']['structure'] == 'fablabMIT') {
		    $t_fablabMIT[$j] = $json_data['features'][$i]['properties']['id']; $j++; }
	    if ($json_data['features'][$i]['properties']['structure'] == 'fablab') {
		    $t_fablab[$k] = $json_data['features'][$i]['properties']['id']; $k++; }
	    if ($json_data['features'][$i]['properties']['structure'] == 'makerspace') {
		    $t_makerspace[$l] = $json_data['features'][$i]['properties']['id']; $l++; }
	    if ($json_data['features'][$i]['properties']['structure'] == 'hakerspace') {
		    $t_hakerspace[$m] = $json_data['features'][$i]['properties']['id']; $m++; }
	    if ($json_data['features'][$i]['properties']['structure'] == 'tierslieu') {
		    $t_tierslieu[$n] = $json_data['features'][$i]['properties']['id']; $n++; }
	    if ($json_data['features'][$i]['properties']['structure'] == 'usages') {
		    $t_usages[$o] = $json_data['features'][$i]['properties']['id']; $o++; }
	    if ($json_data['features'][$i]['properties']['structure'] == 'formation') {
		    $t_formation[$p] = $json_data['features'][$i]['properties']['id']; $p++; }
	    if ($json_data['features'][$i]['properties']['structure'] == 'cantine') {
		    $t_cantine[$q] = $json_data['features'][$i]['properties']['id']; $q++; }
	    if ($json_data['features'][$i]['properties']['structure'] == 'frenchtech') {
		    $t_frenchtech[$r] = $json_data['features'][$i]['properties']['id']; $r++; }

	    // => Volume par département
	    if (substr($json_data['features'][$i]['properties']['cp'], 0, 2) == '22') {
		    $t_22[$cpt_22] = $json_data['features'][$i]['properties']['id']; $cpt_22++; }
	    if (substr($json_data['features'][$i]['properties']['cp'], 0, 2) == '29') {
		    $t_29[$cpt_29] = $json_data['features'][$i]['properties']['id']; $cpt_29++; }
	    if (substr($json_data['features'][$i]['properties']['cp'], 0, 2) == '35') {
		    $t_35[$cpt_35] = $json_data['features'][$i]['properties']['id']; $cpt_35++; }
	    if (substr($json_data['features'][$i]['properties']['cp'], 0, 2) == '44') {
		    $t_44[$cpt_44] = $json_data['features'][$i]['properties']['id']; $cpt_44++; }
	    if (substr($json_data['features'][$i]['properties']['cp'], 0, 2) == '56') {
		    $t_56[$cpt_56] = $json_data['features'][$i]['properties']['id']; $cpt_56++; }

	    // => Écosystème numérique
	    // Site web / RSS / Facebook / Twitter
	    if ($json_data['features'][$i]['properties']['web'] != '') {
		    $t_web[$cpt_web] = $json_data['features'][$i]['properties']['id']; $cpt_web++; }
	    if ($json_data['features'][$i]['properties']['rss'] != '') {
		    $t_rss[$cpt_rss] = $json_data['features'][$i]['properties']['id']; $cpt_rss++; }
	    if ($json_data['features'][$i]['properties']['facebook'] != '') {
		    $t_facebook[$cpt_facebook] = $json_data['features'][$i]['properties']['id']; $cpt_facebook++; }
	    if ($json_data['features'][$i]['properties']['twitter'] != '') {
		    $t_twitter[$cpt_twitter] = $json_data['features'][$i]['properties']['id']; $cpt_twitter++; }

	    //=> Concentration VS dispersion (zonage)
	    //$tab_geoloc = array();

  	}

?>
	<?PHP // --- GRAPHIQUES --- // https://live.amcharts.com --- ?>
	<script src="/maplabbzh/amcharts/amcharts.js" type="text/javascript"></script>
	<script src="/maplabbzh/amcharts/serial.js" type="text/javascript"></script>
	<script src="/maplabbzh/amcharts/pie.js" type="text/javascript"></script>
	<script src="/maplabbzh/amcharts/themes/light.js" type="text/javascript"></script>

	<?PHP /* Graphique : Écosystème numérique
                    Couleurs	"#35465C","#f47721","#3B5998","#55ACEE"
                              Web	#35465C
                              RSS	#f47721
                              Facebook	#3B5998
                              Twitter	#55ACEE

                    */
          ?>
          <?PHP // Tableau des données
          $tab_econum_data = ''
	    .'<table style="margin-top:5px;width:100%;" class="stats">'."\n"
	      .'<thead>'."\n"
	         .'<tr>'."\n"
		  .'<th class="milieu"><span class="icon is-large" style="color:#35465C;" title="Sites web / Blogs"><i class="fa fa-globe"></i></span></th>'."\n"
		  .'<th class="milieu"><span class="icon is-large" style="color:#f47721;" title="Syndication de contenus (flux RSS)"><i class="fa fa-rss"></i></span></th>'."\n"
		  .'<th class="milieu"><span class="icon is-large" style="color:#3B5998;" title="Compte Facebook"><i class="fa fa-facebook-official"></i></span></th>'."\n"
		  .'<th class="milieu"><span class="icon is-large" style="color:#55ACEE;" title="Compte Twitter"><i class="fa fa-twitter"></i></span></th>'."\n"
	         .'</tr>'."\n"
	      .'</thead>'."\n"
	      .'<tbody>'."\n"
	         .'<tr>'."\n"
		  .'<td class="milieu font-bold" style="text-align:center;"><span style="background-color:#35465C;color:#FFF;padding:10 10 8 10;border-radius:24px;">'.count($t_web).'</span></td>'."\n"
		  .'<td class="milieu font-bold"><span style="background-color:#f47721;color:#FFF;padding:10 10 8 10;border-radius:24px;">'.count($t_rss).'</span></td>'."\n"
		  .'<td class="milieu font-bold"><span style="background-color:#3B5998;color:#FFF;padding:10 10 8 10;border-radius:24px;">'.count($t_facebook).'</span></td>'."\n"
		  .'<td class="milieu font-bold"><span style="background-color:#55ACEE;color:#FFF;padding:10 10 8 10;border-radius:24px;">'.count($t_twitter).'</span></td>'."\n"
	         .'</tr>'."\n"
	         .'<tr>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">'.round((count($t_web)*100)/$nb_references,0).' %</span></td>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">'.round((count($t_rss)*100)/$nb_references,0).' %</span></td>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">'.round((count($t_facebook)*100)/$nb_references,0).' %</span></td>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">'.round((count($t_twitter)*100)/$nb_references,0).' %</span></td>'."\n"
	         .'</tr>'."\n"
	     .'</tbody>'."\n"
	    .'</table>'."\n";
          ?>
          <script type="text/javascript">
                    AmCharts.makeChart("ecosysteme_numerique",
                              {
                                        "type": "serial",
                                        "categoryField": "category",
                                        "rotate": true,
                                        "urlTarget": "_blank",
                                        "autoResize": false,
                                        "creditsPosition": "bottom-right",
                                        "colors": [
                                                  "#35465C",
                                                  "#f47721",
                                                  "#3B5998",
                                                  "#55ACEE"
                                        ],
                                        "startDuration": 1,
                                        "categoryAxis": {
                                                  "gridPosition": "start",
                                                  "labelsEnabled": false,
                                                  "minorGridAlpha": 0
                                        },
                                        "trendLines": [],
                                        "graphs": [
                                                  {
                                                            "balloonText": "<b>[[value]]</b> pôles (<?PHP echo round((count($t_web)*100)/$nb_references,0); ?>%) ont un <b>[[title]]</b>",
                                                            "fillAlphas": 1,
                                                            "id": "AmGraph-1",
                                                            "title": "Site web",
                                                            "type": "column",
                                                            "valueField": "Site web"
                                                  },
                                                  {
                                                            "balloonText": "<b>[[value]]</b> pôles (<?PHP echo round((count($t_rss)*100)/$nb_references,0); ?>%) ont un <b>[[title]]</b>",
                                                            "fillAlphas": 1,
                                                            "id": "AmGraph-2",
                                                            "title": "Flux RSS",
                                                            "type": "column",
                                                            "valueField": "Syndication (RSS)"
                                                  },
                                                  {
                                                            "balloonText": "<b>[[value]]</b> pôles (<?PHP echo round((count($t_facebook)*100)/$nb_references,0); ?>%) ont un <b>[[title]]</b>",
                                                            "fillAlphas": 1,
                                                            "id": "AmGraph-3",
                                                            "title": "Compte Facebook",
                                                            "type": "column",
                                                            "valueField": "Facebook"
                                                  },
                                                  {
                                                            "balloonText": "<b>[[value]]</b> pôles (<?PHP echo round((count($t_twitter)*100)/$nb_references,0); ?>%) ont un <b>[[title]]</b>",
                                                            "fillAlphas": 1,
                                                            "id": "AmGraph-4",
                                                            "title": "Compte Twitter",
                                                            "type": "column",
                                                            "valueField": "Twitter"
                                                  }
                                        ],
                                        "guides": [],
                                        "valueAxes": [
                                                  {
                                                            "id": "ValueAxis-1",
                                                            "minimum": 0,
                                                            "position": "bottom",
                                                            "precision": 0,
                                                            "autoGridCount": false,
                                                            "gridCount": 10,
                                                            "title": "",
                                                            "titleBold": false
                                                  }
                                        ],
                                        "allLabels": [],
                                        "balloon": {},
                                        "legend": {
                                                  "enabled": false,
                                                  "marginLeft": 0,
                                                  "marginRight": 0,
                                                  "markerType": "circle",
                                                  "useMarkerColorForLabels": true
                                        },
                                        "titles": [
                                                  {
                                                            "id": "Title-1",
                                                            "size": 15,
                                                            "text": ""
                                                  }
                                        ],
                                        "dataProvider": [
                                                  {
                                                            "category": "category 1",
                                                            "Site web": <?PHP echo count($t_web); ?>,
                                                            "Syndication (RSS)": <?PHP echo count($t_rss); ?>,
                                                            "Facebook": <?PHP echo count($t_facebook); ?>,
                                                            "Twitter": <?PHP echo count($t_twitter); ?>
                                                  }
                                        ]
                              }
                    );
          </script>

          <?PHP /* Graphique Répartitions par types de pôles
          Couleurs	"#006699","#0099cc","#66ccff","#6e2585","#ffcc00","#ff6600","#99cc00","#ff3399","#cc0066"
          fablabMIT		Bleu foncé	#006699
          fablab		Bleu		#0099cc
          makerspace	Bleu ciel		#66ccff
          hakerspace	Violet		#6e2585
          tierslieu		Orange		#ffcc00
          usages		Orange foncé	#ff6600
          formation		Vert		#99cc00
          cantine		Rose		#ff3399
          frenchtech	Rose foncé	#cc0066
          */
          ?>
          <?PHP // Tableau des données
          $tab_types_data = ''
	    .'<table style="margin-top:5px;width:100%;" class="stats">'."\n"
	      .'<thead>'."\n"
	         .'<tr>'."\n"
		  .'<th class="milieu"><img src="https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_fablabMIT.png" title="FabLabs MIT" style="width:40px;margin-bottom:5px;" /></th>'."\n"
		  .'<th class="milieu"><img src="https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_fablab.png" title="FabLabs" style="width:40px;margin-bottom:5px;" /></th>'."\n"
		  .'<th class="milieu"><img src="https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_makerspace.png" title="Makerspaces" style="width:40px;margin-bottom:5px;" /></th>'."\n"
		  .'<th class="milieu"><img src="https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_hakerspace.png" title="Hackerspaces" style="width:40px;margin-bottom:5px;" /></th>'."\n"
		  .'<th class="milieu"><img src="https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_tierslieu.png" title="Tiers-Lieux" style="width:40px;margin-bottom:5px;" /></th>'."\n"
		  .'<th class="milieu"><img src="https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_usages.png" title="Usages" style="width:40px;margin-bottom:5px;" /></th>'."\n"
		  .'<th class="milieu"><img src="https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_formation.png" title="Formation & Recherche" style="width:40px;margin-bottom:5px;" /></th>'."\n"
		  .'<th class="milieu"><img src="https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_cantine.png" title="Cantine & coworking" style="width:40px;margin-bottom:5px;" /></th>'."\n"
		  .'<th class="milieu"><img src="https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_frenchtech.png" title="French Tech" style="width:40px;margin-bottom:5px;" /></th>'."\n"
		  .'<th class="milieu">&Sigma;</th>'."\n"
	         .'</tr>'."\n"
	      .'</thead>'."\n"
	      .'<tbody>'."\n"
	         .'<tr>'."\n"
		  .'<td class="milieu font-bold" style="text-align:center;"><span style="background-color:'.$t_couleurs['fablabMIT'].';color:#FFF;padding:10 10 8 10;border-radius:24px;">'.count($t_fablabMIT).'</span></td>'."\n"
		  .'<td class="milieu font-bold"><span style="background-color:'.$t_couleurs['fablab'].';color:#FFF;padding:10 10 8 10;border-radius:24px;">'.count($t_fablab).'</span></td>'."\n"
		  .'<td class="milieu font-bold"><span style="background-color:'.$t_couleurs['makerspace'].';color:#FFF;padding:10 10 8 10;border-radius:24px;">'.count($t_makerspace).'</span></td>'."\n"
		  .'<td class="milieu font-bold"><span style="background-color:'.$t_couleurs['hakerspace'].';color:#FFF;padding:10 10 8 10;border-radius:24px;">'.count($t_hakerspace).'</span></td>'."\n"
		  .'<td class="milieu font-bold"><span style="background-color:'.$t_couleurs['tierslieu'].';color:#FFF;padding:10 10 8 10;border-radius:24px;">'.count($t_tierslieu).'</span></td>'."\n"
		  .'<td class="milieu font-bold"><span style="background-color:'.$t_couleurs['usages'].';color:#FFF;padding:10 10 8 10;border-radius:24px;">'.count($t_usages).'</span></td>'."\n"
		  .'<td class="milieu font-bold"><span style="background-color:'.$t_couleurs['formation'].';color:#FFF;padding:10 10 8 10;border-radius:24px;">'.count($t_formation).'</span></td>'."\n"
		  .'<td class="milieu font-bold"><span style="background-color:'.$t_couleurs['cantine'].';color:#FFF;padding:10 10 8 10;border-radius:24px;">'.count($t_cantine).'</span></td>'."\n"
		  .'<td class="milieu font-bold"><span style="background-color:'.$t_couleurs['frenchtech'].';color:#FFF;padding:10 10 8 10;border-radius:24px;">'.count($t_frenchtech).'</span></td>'."\n"
		  .'<td class="milieu font-bold" title="Nombre total de Pôles recensés"><span style="background-color:#666;color:#FFF;padding:10 10 8 10;border-radius:20px;">'.$nb_references.'</span></td>'."\n"
	         .'</tr>'."\n"
	         .'<tr>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">'.number_format((count($t_fablabMIT) / $nb_references) * 100, 1).' %</span></td>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">'.number_format((count($t_fablab) / $nb_references) * 100, 1).' %</span></td>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">'.number_format((count($t_makerspace) / $nb_references) * 100, 1).' %</span></td>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">'.number_format((count($t_hakerspace) / $nb_references) * 100, 1).' %</span></td>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">'.number_format((count($t_tierslieu) / $nb_references) * 100, 1).' %</span></td>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">'.number_format((count($t_usages) / $nb_references) * 100, 1).' %</span></td>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">'.number_format((count($t_formation) / $nb_references) * 100, 1).' %</span></td>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">'.number_format((count($t_cantine) / $nb_references) * 100, 1).' %</span></td>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">'.number_format((count($t_frenchtech) / $nb_references) * 100, 1).' %</span></td>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">100%</span></td>'."\n"
	         .'</tr>'."\n"
	     .'</tbody>'."\n"
	    .'</table>'."\n";
          ?>

          <script type="text/javascript">
                    AmCharts.makeChart("repartition_types",
                              {
                                        "type": "pie",
                                        "balloonText": "<strong>[[title]]</strong><br /><span style='font-size:14px'><b>[[value]]</b> pôles ([[percents]]%)</span>",
                                        "labelRadius": 76,
                                        "labelText": "[[title]]",
                                        "pullOutRadius": "0%",
                                        "colors": [
                                                  "<?PHP echo $t_couleurs['fablabMIT']; ?>",
                                                  "<?PHP echo $t_couleurs['fablab']; ?>",
                                                  "<?PHP echo $t_couleurs['makerspace']; ?>",
                                                  "<?PHP echo $t_couleurs['hakerspace']; ?>",
                                                  "<?PHP echo $t_couleurs['tierslieu']; ?>",
                                                  "<?PHP echo $t_couleurs['usages']; ?>",
                                                  "<?PHP echo $t_couleurs['formation']; ?>",
                                                  "<?PHP echo $t_couleurs['cantine']; ?>",
                                                  "<?PHP echo $t_couleurs['frenchtech']; ?>"
                                        ],
                                        "labelsEnabled": false,
                                        "marginBottom": 0,
                                        "marginTop": 0,
                                        "titleField": "category",
                                        "urlTarget": "_blank",
                                        "valueField": "column-1",
                                        "autoResize": false,
                                        "creditsPosition": "bottom-right",
                                        "decimalSeparator": ",",
                                        "percentPrecision": 1,
                                        "thousandsSeparator": " ",
                                        "allLabels": [],
                                        "balloon": {},
                                        "titles": [],
                                        "dataProvider": [
                                                  {
                                                            "category": "FabLabs MIT",
                                                            "column-1": "<?PHP echo count($t_fablabMIT); ?>"
                                                  },
                                                  {
                                                            "category": "FabLabs",
                                                            "column-1": "<?PHP echo count($t_fablab); ?>"
                                                  },
                                                  {
                                                            "category": "Makerspaces",
                                                            "column-1": "<?PHP echo count($t_makerspace); ?>"
                                                  },
                                                  {
                                                            "category": "Hackerspaces",
                                                            "column-1": "<?PHP echo count($t_hakerspace); ?>"
                                                  },
                                                  {
                                                            "category": "Tiers-Lieux",
                                                            "column-1": "<?PHP echo count($t_tierslieu); ?>"
                                                  },
                                                  {
                                                            "category": "Usages",
                                                            "column-1": "<?PHP echo count($t_usages); ?>"
                                                  },
                                                  {
                                                            "category": "Formation & Recherche",
                                                            "column-1": "<?PHP echo count($t_formation); ?>"
                                                  },
                                                  {
                                                            "category": "Cantine & Coworking",
                                                            "column-1": "<?PHP echo count($t_cantine); ?>"
                                                  },
                                                  {
                                                            "category": "French Tech",
                                                            "column-1": "<?PHP echo count($t_frenchtech); ?>"
                                                  }
                                        ]
                              }
                    );
          </script>

          <?PHP 	// Graphique : Volumes par départements

          	// Tableau des données
          $tab_departements_data = ''
	    .'<table style="margin-top:5px;width:100%;" class="stats">'."\n"
	      .'<thead>'."\n"
	         .'<tr>'."\n"
		  .'<th class="milieu"><span style="font-bold">22</span><br /><span style="font-size:0.8em;">Côtes d\'Armor</span></th>'."\n"
		  .'<th class="milieu"><span style="font-bold">29</span><br /><span style="font-size:0.8em;">Finistère</span></th>'."\n"
		  .'<th class="milieu"><span style="font-bold">35</span><br /><span style="font-size:0.8em;">Ille-et-Vilaine</span></th>'."\n"
		  .'<th class="milieu"><span style="font-bold">44</span><br /><span style="font-size:0.8em;">Pays-de-la-Loire</span></th>'."\n"
		  .'<th class="milieu"><span style="font-bold">56</span><br /><span style="font-size:0.8em;">Morbihan</span></th>'."\n"
	         .'</tr>'."\n"
	      .'</thead>'."\n"
	      .'<tbody>'."\n"
	         .'<tr>'."\n"
		  .'<td class="milieu font-bold" style="text-align:center;"><span style="background-color:#8a7967;color:#FFF;padding:10 10 8 10;border-radius:24px;">'.count($t_22).'</span></td>'."\n"
		  .'<td class="milieu font-bold"><span style="background-color:#8a7967;color:#FFF;padding:10 10 8 10;border-radius:24px;">'.count($t_29).'</span></td>'."\n"
		  .'<td class="milieu font-bold"><span style="background-color:#8a7967;color:#FFF;padding:10 10 8 10;border-radius:24px;">'.count($t_35).'</span></td>'."\n"
		  .'<td class="milieu font-bold"><span style="background-color:#8a7967;color:#FFF;padding:10 10 8 10;border-radius:24px;">'.count($t_44).'</span></td>'."\n"
		  .'<td class="milieu font-bold"><span style="background-color:#8a7967;color:#FFF;padding:10 10 8 10;border-radius:24px;">'.count($t_56).'</span></td>'."\n"
	         .'</tr>'."\n"
	         .'<tr>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">'.round((count($t_22)*100)/$nb_references,0).' %</span></td>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">'.round((count($t_29)*100)/$nb_references,0).' %</span></td>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">'.round((count($t_35)*100)/$nb_references,0).' %</span></td>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">'.round((count($t_44)*100)/$nb_references,0).' %</span></td>'."\n"
		  .'<td class="milieu"><span style="background-color:#DDD;color:#333;padding:10 10 8 10;border-radius:24px;">'.round((count($t_56)*100)/$nb_references,0).' %</span></td>'."\n"
	         .'</tr>'."\n"
	     .'</tbody>'."\n"
	    .'</table>'."\n";
          ?>
          <script type="text/javascript">
                    AmCharts.makeChart("departements",
                              {
                                        "type": "serial",
                                        "categoryField": "category",
                                        "marginBottom": 0,
                                        "marginLeft": 0,
                                        "marginRight": 0,
                                        "marginTop": 0,
                                        "colors": [
                                                  "#8a7967"
                                                  /*"<?PHP echo $t_couleurs['fablabMIT']; ?>",
                                                  "<?PHP echo $t_couleurs['fablab']; ?>",
                                                  "<?PHP echo $t_couleurs['makerspace']; ?>",
                                                  "<?PHP echo $t_couleurs['hakerspace']; ?>",
                                                  "<?PHP echo $t_couleurs['tierslieu']; ?>",
                                                  "<?PHP echo $t_couleurs['usages']; ?>",
                                                  "<?PHP echo $t_couleurs['formation']; ?>",
                                                  "<?PHP echo $t_couleurs['cantine']; ?>",
                                                  "<?PHP echo $t_couleurs['frenchtech']; ?>"*/
                                        ],
                                        "startDuration": 1,
                                        "urlTarget": "_blank",
                                        "autoResize": false,
                                        "creditsPosition": "bottom-right",
                                        "decimalSeparator": ",",
                                        "percentPrecision": 1,
                                        "thousandsSeparator": " ",
                                        "categoryAxis": {
                                                  "gridPosition": "start",
                                                  "autoGridCount": false,
                                                  "boldLabels": true,
                                                  "gridThickness": 0
                                        },
                                        "trendLines": [],
                                        "graphs": [
                                                  {
                                                            "balloonText": "<b>[[name]]</b> :  [[value]] pôles",
                                                            "fillAlphas": 1,
                                                            "id": "AmGraph-1",
                                                            "title": "Titre",
                                                            "type": "column",
                                                            "valueField": "value"
                                                  }
                                                  /*{
                                                            "balloonText": "<b>[[category]]</b> | <b>[[title]]</b> :  [[value]]",
                                                            "fillAlphas": 1,
                                                            "id": "AmGraph-1",
                                                            "title": "FabLabsMIT",
                                                            "type": "column",
                                                            "valueField": "FabLabs MIT"
                                                  },
                                                  {
                                                            "balloonText": "<b>[[category]]</b> | <b>[[title]]</b> :  [[value]]",
                                                            "fillAlphas": 1,
                                                            "id": "AmGraph-2",
                                                            "title": "FabLabs",
                                                            "type": "column",
                                                            "valueField": "FabLabs"
                                                  },
                                                  {
                                                            "balloonText": "<b>[[category]]</b> | <b>[[title]]</b> :  [[value]]",
                                                            "fillAlphas": 1,
                                                            "id": "AmGraph-3",
                                                            "tabIndex": 2,
                                                            "title": "Makerspaces",
                                                            "type": "column",
                                                            "valueField": "Makerspaces"
                                                  },
                                                  {
                                                            "balloonText": "<b>[[category]]</b> | <b>[[title]]</b> :  [[value]]",
                                                            "fillAlphas": 1,
                                                            "id": "AmGraph-4",
                                                            "tabIndex": 1,
                                                            "title": "Hakerspaces",
                                                            "type": "column",
                                                            "valueField": "Hakerspaces"
                                                  },
                                                  {
                                                            "balloonText": "<b>[[category]]</b> | <b>[[title]]</b> :  [[value]]",
                                                            "fillAlphas": 1,
                                                            "id": "AmGraph-5",
                                                            "title": "Tiers-Lieux",
                                                            "type": "column",
                                                            "valueField": "Tiers-Lieux"
                                                  },
                                                  {
                                                            "balloonText": "<b>[[category]]</b> | <b>[[title]]</b> :  [[value]]",
                                                            "fillAlphas": 1,
                                                            "id": "AmGraph-6",
                                                            "title": "Usages",
                                                            "type": "column",
                                                            "valueField": "Usages"
                                                  },
                                                  {
                                                            "balloonText": "<b>[[category]]</b> | <b>[[title]]</b> :  [[value]]",
                                                            "fillAlphas": 1,
                                                            "id": "AmGraph-7",
                                                            "title": "Formations & Recherche",
                                                            "type": "column",
                                                            "valueField": "Formation"
                                                  },
                                                  {
                                                            "balloonText": "<b>[[category]]</b> | <b>[[title]]</b> :  [[value]]",
                                                            "fillAlphas": 1,
                                                            "id": "AmGraph-8",
                                                            "title": "Cantines & Coworking",
                                                            "type": "column",
                                                            "valueField": "Cantine"
                                                  },
                                                  {
                                                            "balloonText": "<b>[[category]]</b> | <b>[[title]]</b> :  [[value]]",
                                                            "fillAlphas": 1,
                                                            "id": "AmGraph-9",
                                                            "title": "FrenchTech",
                                                            "type": "column",
                                                            "valueField": "FrenchTech"
                                                  }*/
                                        ],
                                        "guides": [],
                                        "valueAxes": [
                                                  {
                                                            "id": "ValueAxis-1",
                                                            "stackType": "regular",
                                                            "autoGridCount": false,
                                                            "title": "",
                                                            "titleBold": false
                                                  },
                                                  {
                                                            "id": "ValueAxis-2",
                                                            "title": ""
                                                  }
                                        ],
                                        "allLabels": [],
                                        "balloon": {},
                                        "legend": {
                                                  "enabled": false,
                                                  "useGraphSettings": true
                                        },
                                        "titles": [
                                                  {
                                                            "id": "Title-1",
                                                            "size": 15,
                                                            "text": ""
                                                  }
                                        ],
                                        "dataProvider": [
                                                  {
                                                            "category": "22",
                                                            "name": "Côtes-d'Armor",
                                                            "value": <?PHP echo count($t_22); ?>
                                                            /*"FabLabs MIT": 8,
                                                            "FabLabs": 5,
                                                            "Makerspaces": 15,
                                                            "Hakerspaces": "9",
                                                            "Tiers-Lieux": "11",
                                                            "Usages": "8",
                                                            "Formation": "6",
                                                            "Cantine": "8",
                                                            "FrenchTech": 12*/
                                                  },
                                                  {
                                                            "category": "29",
                                                            "name": "Finistère",
                                                            "value": <?PHP echo count($t_29); ?>
                                                            /*"FabLabs MIT": 6,
                                                            "FabLabs": 7,
                                                            "Makerspaces": 7,
                                                            "Hakerspaces": "6",
                                                            "Tiers-Lieux": "2",
                                                            "Usages": "7",
                                                            "Formation": "7",
                                                            "Cantine": "6",
                                                            "FrenchTech": "3"*/
                                                  },
                                                  {
                                                            "category": "35",
                                                            "name": "Ille-et-Vilaine",
                                                            "value": <?PHP echo count($t_35); ?>
                                                            /*"FabLabs MIT": "8",
                                                            "FabLabs": "9",
                                                            "Makerspaces": "7",
                                                            "Hakerspaces": "12",
                                                            "Tiers-Lieux": "15",
                                                            "Usages": "1",
                                                            "Formation": "1",
                                                            "Cantine": "3",
                                                            "FrenchTech": "2"*/
                                                  },
                                                  {
                                                            "category": "44",
                                                            "name": "Pays-de-la-Loire",
                                                            "value": <?PHP echo count($t_44); ?>
                                                            /*"FabLabs MIT": "7",
                                                            "FabLabs": "12",
                                                            "Makerspaces": "11",
                                                            "Hakerspaces": "3",
                                                            "Tiers-Lieux": "5",
                                                            "Usages": "3",
                                                            "Formation": "3",
                                                            "Cantine": "4",
                                                            "FrenchTech": "5"*/
                                                  },
                                                  {
                                                            "category": "56",
                                                            "name": "Morbihan",
                                                            "value": <?PHP echo count($t_56); ?>
                                                            /*"FabLabs MIT": "4",
                                                            "FabLabs": "5",
                                                            "Makerspaces": "3",
                                                            "Hakerspaces": "2",
                                                            "Tiers-Lieux": "4",
                                                            "Usages": "5",
                                                            "Formation": "3",
                                                            "Cantine": "4",
                                                            "FrenchTech": "20"*/
                                                  }
                                        ]
                              }
                    );
          </script>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Carte des FabLabs & Tiers-Lieux de Bretagne #MapLabBZH</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">

          <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bulma.css">

          <?PHP // Bulma Main JS Library ?>
	<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<section class="hero is-dark is-bold">
	<?PHP require_once ('maplabbzh_menutop.inc.php'); // Menu Top ?>
          </section>

          <?PHP // BREADCRUMB ----------- ?>
          <section>
          <nav class="breadcrumb is-medium is-right" aria-label="breadcrumbs">
            <ul class="font-black" style="background-color: #FFCC2F;color:#FFF;">
              <li><a href="/maplabbzh/maplabbzh_liste.php" target="_self" style="color:#FFF;"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M19 12H6M12 5l-7 7 7 7"/></svg> &nbsp; Retour à la liste</span></a></li>
            </ul>
          </nav>
          </section>

          <?PHP // NOM ----------- ?>
          <section class="hero is-light">
            <div class="hero-body">
              <div class="container">
                <h1 class="title" style="font-size:3em;text-transform:uppercase;"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24" fill="none" stroke="#333" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M3 3v18h18"/><path d="M18.7 8l-5.1 5.2-2.8-2.7L7 14.3"/></svg> Data Lab</h1>
              </div>
            </div>
          </section>

          <section style="padding:20px;">
            <p class="title is-2">Pôles</p>
            <p class="subtitle is-4">Répartitions par types de pôles</p>
              <p class="content">
                <div id="repartition_types" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
                <div class="notification is-light no-mobile" style="margin-top:20px;color:#DDD;">
                  <span class="title is-5">Tableau de données</span>
                  <?PHP echo $tab_types_data; ?>
                </div>
              </p>
          </section>

          <section style="padding:20px;">
            <div class="columns is-desktop">
              <div class="column is-success">
                <p class="title is-2">Départements</p>
                <p class="subtitle is-4">Nombre de pôles par départements</p>
                  <p class="content">
                    <div id="departements" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
                    <div class="notification is-light no-mobile" style="margin-top:20px;color:#DDD;">
                      <span class="title is-5">Tableau de données</span>
                      <?PHP echo $tab_departements_data; ?>
                    </div>
                  </p>
              </div>
              <div class="column is-warning">
                <p class="title is-2">Écosystème numérique</p>
                <p class="subtitle is-4">Volumes pour chaque support numérique</p>
                  <p class="content">
                    <div id="ecosysteme_numerique" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
                    <div class="notification is-light no-mobile" style="margin-top:20px;color:#DDD;">
                      <span class="title is-5">Tableau de données</span>
                      <?PHP echo $tab_econum_data; ?>
                    </div>
                  </p>
              </div>
            </div>
          </section>

          <div id="page">

	</div> <?PHP // FIN page ?>


	<?PHP 	// Menu top
require_once ('maplabbzh_footer.inc.php');
?>

</body>
</html>
