<?PHP

/*	#MapLabBZH La carte des FabLabs & autres lieux de dissémination des usages numériques en Bretagne
	=================================================================================================
		Auteur	Guillaume Rouan | @grouan
		Blog	https://grouan.fr/
	-------------------------------------------------------------------------------------------------
		Titre	maplabbzh_submit.php
		Version	v.4 - juillet 2022
		Sources 	https://gitlab.com/grouan/maplabbzh/
		Licence	CC-BY
		Made with CSS Framework Bulma + Font Awesome
	-------------------------------------------------------------------------------------------------
*/

	// Type de formulaire [cat]
	$tab_form = array ('ajouter_new','ajouter_infos','supprimer','modifier','suggerer','autre');
	$tab_form_desc = array ('Nouveau Pôle','Nouvelles infos','Supprimer Pôle','Modifier Pôle','Suggérer une amélioration','Autre...');
	$tab_couleurs = array ('ajouter_new' => 'C1D82F', 'ajouter_infos' => 'FBB034', 'supprimer' => 'CE1126', 'modifier' => 'EA4C89', 'suggerer' => '1CC7D0', 'autre' => 'C68143');
	// Type de formulaire [cat]
	$key = array_search($_GET['cat'], $tab_form);
	$description = $tab_form_desc[$key];
	// Nom du Pôle
	if (isset($_POST['identifiant'])) {
		$json_source = file_get_contents('https://gitlab.com/grouan/maplabbzh/raw/master/map_bzh_fablab.geojson');
		$json_data = json_decode($json_source, true);  // Dans un tableau
		$nb_references = count ($json_data['features']);
		for ($i=0; $i < $nb_references; $i++) {
			if ($json_data['features'][$i]['properties']['id'] == $_POST['identifiant']) {
				$nom_du_pole = $json_data['features'][$i]['properties']['name'];
				$type_de_pole = $json_data['features'][$i]['properties']['structure'];
			}
		}
	}
	// Titre
	$texte = '<DIV style="background-color:#'.$tab_couleurs[$_GET['cat']].';color:#FFF;text-align:center;padding:20px;margin-bottom:20px;"><h1>'.$description.'</h1></DIV>'."\n";

if ($_GET['cat'] == 'ajouter_new') { // Créer une nouvelle fiche  ——————————————————————————————————————————————————————————

	// Auteur du message
	$texte .= '<DIV style="background-color:#EAEAEA;padding:20px;">';
	$texte .= '<div style="font-weight:bold;">'.$_POST['contact_nom'].' ('.$_POST['contact_statut'].')</div>'."\n";
	$texte .= '<div><a href="mailto:'.$_POST['contact_email'].'">'.$_POST['contact_email'].'</a></div>'."\n";
	$texte .= '<div style="color:#666">IP { '.$_POST['adresse_ip'].' }<br />NAV { '.$_POST['navigateur'].' }</div>'."\n";
	$texte .= '</DIV>';
	$texte .= '<DIV style="padding:20px;margin-bottom:20px;border:3px solid #EAEAEA;">';
	$texte .= '<div>'.nl2br($_POST['contact_message']).'</div>'."\n";
	$texte .= '</DIV>';
	// Pôle
	$texte .= '<DIV style="background-color:#333;color:#FFF;text-align:center;padding:10px;"><h1>'.$_POST['_nom'].'</h1></DIV>'."\n";
	$texte .= '<DIV style="padding:20px;margin-bottom:20px;border:3px solid #333;">';
if (!empty($_POST['_nom'])) { $texte .= '<div>NOM <strong>'.$_POST['_nom'].'</strong></div>'."\n"; }
if (!empty($_POST['_organisme'])) { $texte .= '<div>ORG : '.$_POST['_organisme'].'</div>'."\n"; }
if (!empty($_POST['_type'])) { $texte .= '<div>TYPE : '.$_POST['_type'].'</div>'."\n"; }
if (!empty($_POST['_adresse']) || !empty($_POST['_cp']) || !empty($_POST['_ville'])) { $texte .= '<div>ADR : '.$_POST['_adresse'].'<br />'.$_POST['_cp'].' '.$_POST['_ville'].'</div>'."\n"; }
if (!empty($_POST['_email'])) { $texte .= '<div><a href="mailto:'.$_POST['_email'].'">'.$_POST['_email'].'</a></div>'."\n"; }
if (!empty($_POST['_web'])) { $texte .=	'<div>WEB : <a href="'.$_POST['_web'].'" target="_blank">'.$_POST['_web'].'</a></div>'."\n"; }
if (!empty($_POST['_rss'])) { $texte .=	'<div>RSS : <a href="'.$_POST['_rss'].'" target="_blank">'.$_POST['_rss'].'</a></div>'."\n"; }
if (!empty($_POST['_facebook'])) { $texte .= '<div>FB : <a href="https://facebook.com/'.$_POST['_facebook'].'" target="_blank">'.$_POST['_facebook'].'</a></div>'."\n"; }
if (!empty($_POST['_twitter'])) { $texte .= '<div>TW : <a href="https://twitter.com/'.$_POST['_twitter'].'" target="_blank">'.$_POST['_twitter'].'</a></div>'."\n"; }
	//$texte .=	'<div><a href="'.$_POST[''].'" target="_blank">'.$_POST[''].'</a></div>'."\n";
if (!empty($_POST['_GPSlon']) || !empty($_POST['_GPSlat'])) { $texte .=	'<div>GPS : <a href="https://www.openstreetmap.org/search?query='.$_POST['_GPSlat'].'%2C%20'.$_POST['_GPSlon'].'#map=17/'.$_POST['_GPSlat'].'/'.$_POST['_GPSlon'].'" target="_blank">'.$_POST['_GPSlon'].' , '.$_POST['_GPSlat'].'</a></div>'."\n"; }
	$texte .= '</DIV>';

} if ($_GET['cat'] == 'ajouter_infos') { // Ajouter des infos complémentaires à une fiche [id] —————————————————————————————————

	// Auteur du message
	$texte .= '<DIV style="background-color:#EAEAEA;padding:20px;">';
	$texte .= '<div style="font-weight:bold;">'.$_POST['contact_nom'].' ('.$_POST['contact_statut'].')</div>'."\n";
	$texte .= '<div><a href="mailto:'.$_POST['contact_email'].'">'.$_POST['contact_email'].'</a></div>'."\n";
	$texte .= '<div style="color:#666">IP { '.$_POST['adresse_ip'].' }<br />NAV { '.$_POST['navigateur'].' }</div>'."\n";
	$texte .= '</DIV>';
	$texte .= '<DIV style="padding:20px;margin-bottom:20px;border:3px solid #EAEAEA;">';
	$texte .= '<div>'.nl2br($_POST['contact_message']).'</div>'."\n";
	$texte .= '</DIV>';
	// Pôle
	$texte .= '<DIV style="background-color:#333;color:#FFF;text-align:center;padding:10px;"><h1>'.$nom_du_pole.'</h1></DIV>'."\n";
	$texte .= '<DIV style="padding:20px;margin-bottom:20px;border:3px solid #333;">';
if (!empty($_POST['_nom'])) { $texte .= '<div>NOM <strong>'.$_POST['_nom'].'</strong></div>'."\n"; }
if (!empty($_POST['_organisme'])) { $texte .= '<div>ORG : '.$_POST['_organisme'].'</div>'."\n"; }
if (!empty($_POST['_type'])) { $texte .= '<div>TYPE : '.$_POST['_type'].'</div>'."\n"; }
if (!empty($_POST['_adresse']) || !empty($_POST['_cp']) || !empty($_POST['_ville'])) { $texte .= '<div>ADR : '.$_POST['_adresse'].'<br />'.$_POST['_cp'].' '.$_POST['_ville'].'</div>'."\n"; }
if (!empty($_POST['_email'])) { $texte .= '<div><a href="mailto:'.$_POST['_email'].'">'.$_POST['_email'].'</a></div>'."\n"; }
if (!empty($_POST['_web'])) { $texte .=	'<div>WEB : <a href="'.$_POST['_web'].'" target="_blank">'.$_POST['_web'].'</a></div>'."\n"; }
if (!empty($_POST['_rss'])) { $texte .=	'<div>RSS : <a href="'.$_POST['_rss'].'" target="_blank">'.$_POST['_rss'].'</a></div>'."\n"; }
if (!empty($_POST['_facebook'])) { $texte .= '<div>FB : <a href="https://facebook.com/'.$_POST['_facebook'].'" target="_blank">'.$_POST['_facebook'].'</a></div>'."\n"; }
if (!empty($_POST['_twitter'])) { $texte .= '<div>TW : <a href="https://twitter.com/'.$_POST['_twitter'].'" target="_blank">'.$_POST['_twitter'].'</a></div>'."\n"; }
	//$texte .=	'<div><a href="'.$_POST[''].'" target="_blank">'.$_POST[''].'</a></div>'."\n";
if (!empty($_POST['_GPSlon']) || !empty($_POST['_GPSlat'])) { $texte .=	'<div>GPS : <a href="https://www.openstreetmap.org/search?query='.$_POST['_GPSlat'].'%2C%20'.$_POST['_GPSlon'].'#map=17/'.$_POST['_GPSlat'].'/'.$_POST['_GPSlon'].'" target="_blank">'.$_POST['_GPSlon'].' , '.$_POST['_GPSlat'].'</a></div>'."\n"; }
	$texte .= '</DIV>';
	$texte .= '<a href="https://grouan.fr/maplabbzh/maplabbzh_fiche.php?id='.$_POST['identifiant'].'&type='.$type_de_pole.'" target="_blank" style="text-decoration:none;color:#333;"><DIV style="background-color:#EAEAEA;padding:5px;margin-bottom:20px;text-align:center;">&rarr; voir la fiche #MapLabBzh</DIV></a>';

} if ($_GET['cat'] == 'supprimer') { // Supprimer une fiche [id] ——————————————————————————————————————————————————————————

	// Auteur du message
	$texte .= '<DIV style="background-color:#EAEAEA;padding:20px;">';
	$texte .= '<div style="font-weight:bold;">'.$_POST['contact_nom'].' ('.$_POST['contact_statut'].')</div>'."\n";
	$texte .= '<div><a href="mailto:'.$_POST['contact_email'].'">'.$_POST['contact_email'].'</a></div>'."\n";
	$texte .= '<div style="color:#666">IP { '.$_POST['adresse_ip'].' }<br />NAV { '.$_POST['navigateur'].' }</div>'."\n";
	$texte .= '</DIV>';
	$texte .= '<DIV style="padding:20px;margin-bottom:20px;border:3px solid #EAEAEA;">';
	$texte .= '<div>'.nl2br($_POST['contact_message']).'</div>'."\n";
	$texte .= '</DIV>';
	// Pôle
	$texte .= '<DIV style="background-color:#333;color:#FFF;text-align:center;padding:10px;"><h1>'.$nom_du_pole.'</h1></DIV>'."\n";
	$texte .= '<a href="https://grouan.fr/maplabbzh/maplabbzh_fiche.php?id='.$_POST['identifiant'].'&type='.$type_de_pole.'" target="_blank" style="text-decoration:none;color:#333;"><DIV style="background-color:#EAEAEA;padding:5px;margin-bottom:20px;text-align:center;">&rarr; voir la fiche #MapLabBzh &larr;</DIV></a>';

} if ($_GET['cat'] == 'modifier') { // Modifier une fiche [id] ———————————————————————————————————————————————————————————————

	// Auteur du message
	$texte .= '<DIV style="background-color:#EAEAEA;padding:20px;">';
	$texte .= '<div style="font-weight:bold;">'.$_POST['contact_nom'].' ('.$_POST['contact_statut'].')</div>'."\n";
	$texte .= '<div><a href="mailto:'.$_POST['contact_email'].'">'.$_POST['contact_email'].'</a></div>'."\n";
	$texte .= '<div style="color:#666">IP { '.$_POST['adresse_ip'].' }<br />NAV { '.$_POST['navigateur'].' }</div>'."\n";
	$texte .= '</DIV>';
	$texte .= '<DIV style="padding:20px;margin-bottom:20px;border:3px solid #EAEAEA;">';
	$texte .= '<div>'.nl2br($_POST['contact_message']).'</div>'."\n";
	$texte .= '</DIV>';
	// Pôle
	$texte .= '<DIV style="background-color:#333;color:#FFF;text-align:center;padding:10px;"><h1>'.$nom_du_pole.'</h1></DIV>'."\n";
	$texte .= '<DIV style="padding:20px;margin-bottom:20px;border:3px solid #333;">';
if (!empty($_POST['_nom'])) { $texte .= '<div>NOM <strong>'.$_POST['_nom'].'</strong></div>'."\n"; }
if (!empty($_POST['_organisme'])) { $texte .= '<div>ORG : '.$_POST['_organisme'].'</div>'."\n"; }
if (!empty($_POST['_type'])) { $texte .= '<div>TYPE : '.$_POST['_type'].'</div>'."\n"; }
if (!empty($_POST['_adresse']) || !empty($_POST['_cp']) || !empty($_POST['_ville'])) { $texte .= '<div>ADR : '.$_POST['_adresse'].'<br />'.$_POST['_cp'].' '.$_POST['_ville'].'</div>'."\n"; }
if (!empty($_POST['_email'])) { $texte .= '<div><a href="mailto:'.$_POST['_email'].'">'.$_POST['_email'].'</a></div>'."\n"; }
if (!empty($_POST['_web'])) { $texte .=	'<div>WEB : <a href="'.$_POST['_web'].'" target="_blank">'.$_POST['_web'].'</a></div>'."\n"; }
if (!empty($_POST['_rss'])) { $texte .=	'<div>RSS : <a href="'.$_POST['_rss'].'" target="_blank">'.$_POST['_rss'].'</a></div>'."\n"; }
if (!empty($_POST['_facebook'])) { $texte .= '<div>FB : <a href="https://facebook.com/'.$_POST['_facebook'].'" target="_blank">'.$_POST['_facebook'].'</a></div>'."\n"; }
if (!empty($_POST['_twitter'])) { $texte .= '<div>TW : <a href="https://twitter.com/'.$_POST['_twitter'].'" target="_blank">'.$_POST['_twitter'].'</a></div>'."\n"; }
	//$texte .=	'<div><a href="'.$_POST[''].'" target="_blank">'.$_POST[''].'</a></div>'."\n";
if (!empty($_POST['_GPSlon']) || !empty($_POST['_GPSlat'])) { $texte .=	'<div>GPS : <a href="https://www.openstreetmap.org/search?query='.$_POST['_GPSlat'].'%2C%20'.$_POST['_GPSlon'].'#map=17/'.$_POST['_GPSlat'].'/'.$_POST['_GPSlon'].'" target="_blank">'.$_POST['_GPSlon'].' , '.$_POST['_GPSlat'].'</a></div>'."\n"; }
	$texte .= '</DIV>';
	$texte .= '<a href="https://grouan.net/maplabbzh/maplabbzh_fiche.php?id='.$_POST['identifiant'].'&type='.$type_de_pole.'" target="_blank" style="text-decoration:none;color:#333;"><DIV style="background-color:#EAEAEA;padding:5px;margin-bottom:20px;text-align:center;">&rarr; voir la fiche #MapLabBzh</DIV></a>';

} if ($_GET['cat'] == 'suggerer') { // Suggérer une amélioration —————————————————————————————————————————————————————————————

          // Auteur du message
	$texte .= '<DIV style="background-color:#EAEAEA;padding:20px;margin-bottom:20px;">';
	$texte .= '<div style="font-weight:bold;">'.$_POST['contact_nom'].' ('.$_POST['contact_statut'].')</div>'."\n";
	$texte .= '<div><a href="mailto:'.$_POST['contact_email'].'">'.$_POST['contact_email'].'</a></div>'."\n";
	$texte .= '<div style="color:#666">IP { '.$_POST['adresse_ip'].' }<br />NAV { '.$_POST['navigateur'].' }</div>'."\n";
	$texte .= '</DIV>';
	// Objet + Message
	$texte .= '<DIV style="padding:20px;background-color:#EAEAEA;">';
	$texte .= '<div>'.$_POST['contact_objet'].'</div>'."\n";
	$texte .= '</DIV>';
	$texte .= '<DIV style="padding:20px;margin-bottom:20px;border:3px solid #EAEAEA;">';
	$texte .= '<div>'.nl2br($_POST['contact_message']).'</div>'."\n";
	$texte .= '</DIV>';

} if ($_GET['cat'] == 'autre') { // Autre ————————————————————————————————————————————————————————————————————————————————————

          // Auteur du message
	$texte .= '<DIV style="background-color:#EAEAEA;padding:20px;margin-bottom:20px;">';
	$texte .= '<div style="font-weight:bold;">'.$_POST['contact_nom'].' ('.$_POST['contact_statut'].')</div>'."\n";
	$texte .= '<div><a href="mailto:'.$_POST['contact_email'].'">'.$_POST['contact_email'].'</a></div>'."\n";
	$texte .= '<div style="color:#666">IP { '.$_POST['adresse_ip'].' }<br />NAV { '.$_POST['navigateur'].' }</div>'."\n";
	$texte .= '</DIV>';
	// Objet + Message
	$texte .= '<DIV style="padding:20px;background-color:#EAEAEA;">';
	$texte .= '<div>'.$_POST['contact_objet'].'</div>'."\n";
	$texte .= '</DIV>';
	$texte .= '<DIV style="padding:20px;margin-bottom:20px;border:3px solid #EAEAEA;">';
	$texte .= '<div>'.nl2br($_POST['contact_message']).'</div>'."\n";
	$texte .= '</DIV>';

}

	// Envoi du courriel
         $to  = 'VOTRE-EMAIL'; 	// 'abc@def.com' . ', ';
         				// $to .= 'wez@example.com';

         // Sujet
         $subject = '#MapLabBzh > '.$description;

         // message
         $message = '
         <html>
	<head>
	 <title>#MapLabBzh Contribution</title>
	</head>
	<body>';
	$message .= $texte;
	 $message .= '
	</body>
         </html>
         ';

         // Pour envoyer un mail HTML, l'en-tête Content-type doit être défini
         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

         // En-têtes additionnels
         $headers .= 'To: Guillaume <'.$to.'>' . "\r\n";
         $headers .= 'From: '.$_POST['contact_nom'].' <'.$_POST['contact_email'].'>' . "\r\n";
         //$headers .= 'Cc: mail@example.com' . "\r\n";
         //$headers .= 'Bcc: mail2@example.com' . "\r\n";

         // Envoi
         mail($to, $subject, $message, $headers);

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Carte des FabLabs & Tiers-Lieux de Bretagne #MapLabBZH</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">

          <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bulma.css">

          <?PHP // Bulma Main JS Library ?>
	<script type="text/javascript" src="js/main.js"></script>


</head>

<body>

          <section class="hero is-dark is-bold">
	<?PHP require_once ('maplabbzh_menutop.inc.php'); // Menu Top ?>
          </section>

          <?PHP // BREADCRUMB ----------- ?>
          <section>
          <nav class="breadcrumb is-medium is-right" aria-label="breadcrumbs"> <?PHP // Accueil Liste ?>
            <ul class="font-black" style="background-color:#FFCC2F;color:#FFF;">
              <li>&nbsp;</li>
            </ul>
          </nav>
          </section>

          <?PHP // NOM ----------- ?>
          <section class="hero is-success">
            <div class="hero-body">
              <div class="container">
                <h1 class="title" style="font-size:3em;text-transform:uppercase;"><span class="icon is-large"><i class="fa fa-handshake-o"></i></span> <span class="font-bold">Merci de votre contribution</span></h1>
                <h2 class="subtitle no-mobile">
                  Votre participation permet réellement de faire avancer la carte !
                </h2>
              </div>
            </div>
          </section>
	<?PHP if ( (isset($_GET['id'])) && (isset($_GET['cat'])) ) { ?>
	<section class="hero is-danger">
            <div class="hero-body">
              <div class="container">
                <h1 class="title">
                  <?PHP echo $_nom; ?>
                </h1>
              </div>
            </div>
          </section>
          <?PHP } else { ?>
          <section style="border-bottom:8px solid #ff3860;margin-top:-25px;">&nbsp;</section>
          <?PHP } ?>

<?PHP if (!isset($_GET['cat'])) { // Erreur ?>

          <section class="">

                    <div>Oups :/ Vous n'êtes pas au bon endroit... <a href="maplabbzh_liste.php" target="_self">Retournez à la liste</a></div>

          </section>

<?PHP } else { //  ?>

	<section class="">

                    <div>&nbsp;</div>

          </section>

	<meta http-equiv="refresh" content="4;url=maplabbzh_liste.php" />

<?PHP } ?>

<?PHP 	// Menu top
require_once ('maplabbzh_footer.inc.php');
?>

</body>
</html>
