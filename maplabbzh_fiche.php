<?PHP

/*	#MapLabBZH La carte des FabLabs & autres lieux de dissémination des usages numériques en Bretagne
	=================================================================================================
		Auteur	Guillaume Rouan | @grouan
		Blog	https://grouan.fr/
	-------------------------------------------------------------------------------------------------
		Titre	maplabbzh_fiche.php
		Version	v.4 - juillet 2022
		Sources 	https://gitlab.com/grouan/maplabbzh/
		Licence	CC-BY
		Made with CSS Framework Bulma + Font Awesome
	-------------------------------------------------------------------------------------------------
*/


	// Package de fonctions
          require_once ('maplabbzh_fonctions.inc.php');

	// Récupération de l'identifiant
	$_identifiant 	= $_GET['id'];
	$_structure	= $_GET['type'];

	// Détecter la géolocalisation
	// Détecter le type de Device
?>

          <script type="text/javascript">
/*
	var isMobile = false; // Initialisation
	// Détection du Device
	if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
	    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { isMobile = true; }
*/
	</script>

<?PHP
	// Sources JSON : Github
	$json_source = file_get_contents('https://gitlab.com/grouan/maplabbzh/raw/master/map_bzh_fablab.geojson');

	//$json_data = json_decode($json_source); // Fichier brut
	$json_data = json_decode($json_source, true);  // Dans un tableau

	// Recherche de l'identifiant dans le fichier JSON
	$nb_references = count ($json_data['features']);

	$j=0; // pour $tab_similaire
	for ($i=0; $i < $nb_references; $i++) {

		if ($json_data['features'][$i]['properties']['id'] == $_identifiant) { // FICHE
			$_nom 		= $json_data['features'][$i]['properties']['name'];
			$_structure 	= $json_data['features'][$i]['properties']['structure'];
			$_adresse 	= $json_data['features'][$i]['properties']['adresse'];
			$_cp 		= $json_data['features'][$i]['properties']['cp'];
			$_ville 		= $json_data['features'][$i]['properties']['ville'];
			$_web 		= $json_data['features'][$i]['properties']['web'];
			$_twitter 	= $json_data['features'][$i]['properties']['twitter'];
			$_facebook 	= $json_data['features'][$i]['properties']['facebook'];
			$_email 		= $json_data['features'][$i]['properties']['email'];
			$_tel 		= $json_data['features'][$i]['properties']['tel'];
			$_rss 		= $json_data['features'][$i]['properties']['rss'];
			$_longitude 	= $json_data['features'][$i]['geometry']['coordinates'][0];
			$_latitude 	= $json_data['features'][$i]['geometry']['coordinates'][1];
		} else {
			if ($json_data['features'][$i]['properties']['structure'] == $_structure) { // SIMILAIRES
				$tab_similaire[$j]['id'] = $json_data['features'][$i]['properties']['id'];
				$tab_similaire[$j]['name'] = $json_data['features'][$i]['properties']['name'];
				$tab_similaire[$j]['ville'] = $json_data['features'][$i]['properties']['ville'];
				$j++;
			}
		}

  	}


?>

<?PHP // Partage de la géolocalisation ?>
	<script>
		function maPosition (position) {
		  //var infopos = "Position déterminée (LAT, LON) = \n";
		  //infopos += position.coords.latitude +", \n";
		  //infopos += position.coords.longitude +"<br /><br />\n";
		  /**/
		  var distance = maDistance (position.coords.latitude, position.coords.longitude, <?PHP echo $_latitude; ?>, <?PHP echo $_longitude; ?>);
		  var infoDist = Math.round(distance);
		  var infopos = "Environ \n";
		  infopos += infoDist + " km \n";
		  document.getElementById("infoposition").innerHTML = infopos;
		}

		if(navigator.geolocation) { navigator.geolocation.getCurrentPosition(maPosition); }

		/* --- */
		function maDistance (lat1, lon1, lat2, lon2) {
			var r = 6366; /* rayon de la Terre ~6371*/
			/* Conversion degrés => radians */
			var pi = Math.PI;
  			var lat1 = lat1 * (pi/180);
			var lon1 = lon1 * (pi/180);
			var lat2 = lat2 * (pi/180);
			var lon2 = lon2 * (pi/180);

			var alt1 = 0; /* Altitude */
			var alt2 = 0;

			/* Calcul */
			var dp = 2 * Math.asin (
					Math.sqrt (
						Math.pow ( Math.sin ( (lat1-lat2)/2 ),2)
						+ Math.cos (lat1) * Math.cos(lat2) * Math.pow ( Math.sin ( (lon1-lon2)/2),2)
					)
				    );
			/* En km */
			var d = dp * r;
			/* Pythagore */
			var distance = Math.sqrt ( Math.pow (d,2) + Math.pow ((alt2-alt1),2) ); /* h */

			return distance;
/*
	sin = Math.sin(x)		cos = Math.cos(x)		asin = Math.asin(x)		sqrt = Math.sqrt(x) // racine carrée
	pow = Math.pow(base,exposant) // expression exponentielle
*/
		}

          </script>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Carte des FabLabs & Tiers-Lieux de Bretagne #MapLabBZH</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">

          <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bulma.css">

          <?PHP // Bulma Main JS Library ?>
	<script type="text/javascript" src="js/main.js"></script>

	<?PHP // Affichage de la mini-carte ?>
	<script src="http://cdn.leafletjs.com/leaflet-0.7.5/leaflet.js"></script>
	<script src="http://www.openlayers.org/api/OpenLayers.js"></script>

          <script>
		ACCESS_TOKEN = 'pk.eyJ1IjoiZ3JvdWFuIiwiYSI6ImNpZmptMmNsczAwenlzeGx4Ym1zOGM5ZzkifQ.uLnuOKagy0bJgfUSPY9hCw';

		CM_ATTR = 'Donn&eacute;es carte &copy; contributeurs <a href="http://openstreetmap.org">OpenStreetMap</a>, ' +
			'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="http://cloudmade.com">CloudMade</a>';

		CM_URL = 'http://{s}.tile.cloudmade.com/d4fc77ea4a63471cab2423e66626cbb6/{styleId}/256/{z}/{x}/{y}.png';

		MB_ATTR = '<a href="http://openstreetmap.org" target="_blank">OpenStreetMap</a> + ' +
			'<a href="http://mapbox.com" target="_blank">Mapbox</a> | ' +
			'<a href="http://guillaume-rouan.net" target="_blank">Guillaume Rouan</a> <a href="http://creativecommons.fr/licences/" target="_blank">CC BY</a>';

		MB_URL = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + ACCESS_TOKEN;

		OSM_URL = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';

		OSM_ATTRIB = '&copy; contributeurs <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> ';
	</script>

</head>

<body>

	<?PHP // FACEBOOK SDK ?>
          	<div id="fb-root"></div>
		    <script>(function(d, s, id) {
                          var js, fjs = d.getElementsByTagName(s)[0];
                          if (d.getElementById(id)) return;
                          js = d.createElement(s); js.id = id;
                          js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.10";
                          fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
          <?PHP // ?>

          <section class="hero is-dark is-bold">
	<?PHP require_once ('maplabbzh_menutop.inc.php'); // Menu Top ?>
          </section>

          <?PHP // BREADCRUMB ----------- ?>
          <section>
          <nav class="breadcrumb is-medium is-right" aria-label="breadcrumbs">
            <ul class="font-black" style="background-color: #FFCC2F;color:#FFF;">
              <li><a href="/maplabbzh/maplabbzh_liste.php" target="_self" style="color:#FFF;"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M19 12H6M12 5l-7 7 7 7"/></svg> &nbsp; Retour à la liste</span></a></li>
            </ul>
          </nav>
          </section>

          <?PHP // NOM ----------- ?>
          <section class="hero is-light">
            <div class="hero-body">
              <div class="container">
                <h1 class="title" style="font-size:3em;text-transform:uppercase;"><img src="https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_<?PHP echo $_structure; ?>.png" style="width:40px;" /> <span style="border-bottom: 8px solid #FFCC2F;"><?PHP echo $_nom; ?></span></h1>
              </div>
            </div>
          </section>

	<section>
                    <div class="tile is-ancestor">
                      <div class="tile is-vertical is-8">
                        <div class="tile">
                          <div class="tile is-parent is-vertical" style="padding: 0.75em 0 0 0;">

                            <?PHP // ADRESSE ----------- ?>
                            <article class="tile is-child notification is-light" style="height:auto;background-color:#FFF;border-radius:0px;margin:0px;">
                              <p class="subtitle">
<?PHP if ($_adresse == '') { $adresse1 = ''; } else { $adresse1 = $_adresse.'<br />'; } ?>
                                <div style="float:left;margin-bottom:20px;"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#333" class="bi bi-geo-alt-fill" viewBox="0 0 16 16">
  <path d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z"/>
</svg> </div>
                                <div style="margin:0px 0px 20px 50px;height:50px;" class="font-black"><?PHP echo $adresse1.$_cp; ?> <span style="text-transform:uppercase;"><?PHP echo $_ville; ?></span></div>
                              </p>
<?PHP if ($_tel != '') { ?>
                              <p class="subtitle">
                                <div style="float:left;margin-bottom:20px;"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#333" class="bi bi-telephone" viewBox="0 0 16 16">
  <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
</svg> </div>
                                <div style="margin:0px 0px 20px 50px;height:50px;" class="font-black"><?PHP echo $_tel; ?></div>
                              </p>
<?PHP } ?>
<?PHP if ($_email != '') { ?>
                              <p class="subtitle">
                                <div style="float:left;margin-bottom:20px;"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#333" class="bi bi-mailbox" viewBox="0 0 16 16">
  <path d="M4 4a3 3 0 0 0-3 3v6h6V7a3 3 0 0 0-3-3zm0-1h8a4 4 0 0 1 4 4v6a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V7a4 4 0 0 1 4-4zm2.646 1A3.99 3.99 0 0 1 8 7v6h7V7a3 3 0 0 0-3-3H6.646z"/>
  <path d="M11.793 8.5H9v-1h5a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.354-.146l-.853-.854zM5 7c0 .552-.448 0-1 0s-1 .552-1 0a1 1 0 0 1 2 0z"/>
</svg> </div>
                                <div style="margin:0px 0px 20px 50px;height:50px;" class="font-black"><a href="mailto:<?PHP echo $_email; ?>" target="_blank" title="contacter par courriel"><?PHP echo $_email; ?></a></div>
                              </p>
<?PHP } ?>
<?PHP if ($_web != '') { ?>
                              <p class="subtitle">
                                <div style="float:left;margin-bottom:20px;"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#333" class="bi bi-globe" viewBox="0 0 16 16">
  <path d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm7.5-6.923c-.67.204-1.335.82-1.887 1.855A7.97 7.97 0 0 0 5.145 4H7.5V1.077zM4.09 4a9.267 9.267 0 0 1 .64-1.539 6.7 6.7 0 0 1 .597-.933A7.025 7.025 0 0 0 2.255 4H4.09zm-.582 3.5c.03-.877.138-1.718.312-2.5H1.674a6.958 6.958 0 0 0-.656 2.5h2.49zM4.847 5a12.5 12.5 0 0 0-.338 2.5H7.5V5H4.847zM8.5 5v2.5h2.99a12.495 12.495 0 0 0-.337-2.5H8.5zM4.51 8.5a12.5 12.5 0 0 0 .337 2.5H7.5V8.5H4.51zm3.99 0V11h2.653c.187-.765.306-1.608.338-2.5H8.5zM5.145 12c.138.386.295.744.468 1.068.552 1.035 1.218 1.65 1.887 1.855V12H5.145zm.182 2.472a6.696 6.696 0 0 1-.597-.933A9.268 9.268 0 0 1 4.09 12H2.255a7.024 7.024 0 0 0 3.072 2.472zM3.82 11a13.652 13.652 0 0 1-.312-2.5h-2.49c.062.89.291 1.733.656 2.5H3.82zm6.853 3.472A7.024 7.024 0 0 0 13.745 12H11.91a9.27 9.27 0 0 1-.64 1.539 6.688 6.688 0 0 1-.597.933zM8.5 12v2.923c.67-.204 1.335-.82 1.887-1.855.173-.324.33-.682.468-1.068H8.5zm3.68-1h2.146c.365-.767.594-1.61.656-2.5h-2.49a13.65 13.65 0 0 1-.312 2.5zm2.802-3.5a6.959 6.959 0 0 0-.656-2.5H12.18c.174.782.282 1.623.312 2.5h2.49zM11.27 2.461c.247.464.462.98.64 1.539h1.835a7.024 7.024 0 0 0-3.072-2.472c.218.284.418.598.597.933zM10.855 4a7.966 7.966 0 0 0-.468-1.068C9.835 1.897 9.17 1.282 8.5 1.077V4h2.355z"/>
</svg> </div>
<?PHP
	if (strpos($_web, 'https') === false) {
		if (strpos($_web, 'www') === false) { $_siteweb = substr($_web, 7); /* http:// */ }
		else { $_siteweb = substr($_web, 11); /* http://www. */ }
	} else {
		if (strpos($_web, 'www') === false) { $_siteweb = substr($_web, 8); /* https:// */ }
		else { $_siteweb = substr($_web, 12); /* https://www. */ }
	}
?>
                                <div style="margin:0px 0px 20px 50px;height:50px;" class="font-black"><a href="<?PHP echo $_web; ?>" target="_blank"><?PHP if (strlen($_siteweb) >= 40) { echo substr($_siteweb,0,40).'&hellip;'; } else { echo $_siteweb; } ?></a></div>
                              </p>

                              <div class="tags">
                                <span class="tag is-danger font-black" style="color:#FFF;"><?PHP echo $_structure; ?></span>
                                <span class="tag is-danger font-black" style="color:#FFF;"><?PHP echo $_ville; ?></span>
                              </div>
<?PHP } ?>
			<div><a class="button is-success is-medium" href="maplabbzh_contribuer.php?id=<?PHP echo $_identifiant; ?>" alt="" title="Ajoutez / Modifiez / Supprimez / Suggérez des infos">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><line x1="20" y1="8" x2="20" y2="14"></line><line x1="23" y1="11" x2="17" y2="11"></line></svg> &nbsp;
				<span class="font-bold">Améliorez cette fiche</span>
			</a></div>

                            </article>

                            <?PHP // GPS ?>
                            <article class="tile is-child notification is-dark" style="border-radius:0px;margin:0px;">
                          <p class="title" style="text-align:center;"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="#FFF" class="bi bi-geo-fill" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M4 4a4 4 0 1 1 4.5 3.969V13.5a.5.5 0 0 1-1 0V7.97A4 4 0 0 1 4 3.999zm2.493 8.574a.5.5 0 0 1-.411.575c-.712.118-1.28.295-1.655.493a1.319 1.319 0 0 0-.37.265.301.301 0 0 0-.057.09V14l.002.008a.147.147 0 0 0 .016.033.617.617 0 0 0 .145.15c.165.13.435.27.813.395.751.25 1.82.414 3.024.414s2.273-.163 3.024-.414c.378-.126.648-.265.813-.395a.619.619 0 0 0 .146-.15.148.148 0 0 0 .015-.033L12 14v-.004a.301.301 0 0 0-.057-.09 1.318 1.318 0 0 0-.37-.264c-.376-.198-.943-.375-1.655-.493a.5.5 0 1 1 .164-.986c.77.127 1.452.328 1.957.594C12.5 13 13 13.4 13 14c0 .426-.26.752-.544.977-.29.228-.68.413-1.116.558-.878.293-2.059.465-3.34.465-1.281 0-2.462-.172-3.34-.465-.436-.145-.826-.33-1.116-.558C3.26 14.752 3 14.426 3 14c0-.599.5-1 .961-1.243.505-.266 1.187-.467 1.957-.594a.5.5 0 0 1 .575.411z"/>
</svg>&nbsp; <?PHP echo number_format($_latitude,4).', '.number_format($_longitude,4); ?></p>
                        </article>

                            <?PHP // DISTANCE ----------- ?>
                        <article class="tile is-child notification is-dark" style="background-color:#FFCC2F;border-radius:0px;margin:0px;">
<?PHP // https://github.com/Project-OSRM/osrm-backend/wiki ?>
                          <p class="title"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="#FFF" class="bi bi-compass-fill" viewBox="0 0 16 16">
  <path d="M15.5 8.516a7.5 7.5 0 1 1-9.462-7.24A1 1 0 0 1 7 0h2a1 1 0 0 1 .962 1.276 7.503 7.503 0 0 1 5.538 7.24zm-3.61-3.905L6.94 7.439 4.11 12.39l4.95-2.828 2.828-4.95z"/>
</svg>&nbsp; distance</p>
                          <p class="subtitle liens" style="font-size:0.9em;">Données établies si vous avez autorisé la géolocalisation par votre navigateur web</p>
                          <p class="content">
                          	<div id="infoposition" class="font-bold" style="font-size: 1.6em;"></div>
                          </p>
                        </article>

                            <?PHP // RESEAUX-SOCIAUX ----------- ?>
<?PHP if ($_twitter != '') { ?>
                            <article class="tile is-child notification is-info" style="height:76px;background-color:#1da1f2;border-radius:0px;margin:0px;padding:auto;"><?PHP // Twitter ?>
                              <p class="title liens"><svg role="img" width="24" height="24" viewBox="0 0 24 24" fill="#FFF" xmlns="http://www.w3.org/2000/svg"><title>Twitter</title><path d="M23.953 4.57a10 10 0 01-2.825.775 4.958 4.958 0 002.163-2.723c-.951.555-2.005.959-3.127 1.184a4.92 4.92 0 00-8.384 4.482C7.69 8.095 4.067 6.13 1.64 3.162a4.822 4.822 0 00-.666 2.475c0 1.71.87 3.213 2.188 4.096a4.904 4.904 0 01-2.228-.616v.06a4.923 4.923 0 003.946 4.827 4.996 4.996 0 01-2.212.085 4.936 4.936 0 004.604 3.417 9.867 9.867 0 01-6.102 2.105c-.39 0-.779-.023-1.17-.067a13.995 13.995 0 007.557 2.209c9.053 0 13.998-7.496 13.998-13.985 0-.21 0-.42-.015-.63A9.935 9.935 0 0024 4.59z"/></svg>&nbsp; <a href="http://twitter.com/<?PHP echo $_twitter; ?>" target="_blank"><?PHP if (strlen($_twitter) >= 17) { echo substr($_twitter,0,17).'&hellip;'; } else { echo $_twitter; } ?></a></p>
                              <a href="https://twitter.com/<?PHP echo $_twitter; ?>" class="twitter-follow-button" data-show-count="true" data-size="large">Follow <?PHP echo $_twitter; ?></a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                            </article>
<?PHP } ?>
<?PHP if ($_facebook != '') { ?>
                            <article class="tile is-child notification is-info" style="height:76px;background-color:#3b5998;border-radius:0px;margin:0px;padding:auto;"><?PHP // Facebook ?>
                              <p class="title liens"><svg role="img" width="24" height="24" viewBox="0 0 24 24" fill="#FFF" xmlns="http://www.w3.org/2000/svg"><title>Facebook</title><path d="M24 12.073c0-6.627-5.373-12-12-12s-12 5.373-12 12c0 5.99 4.388 10.954 10.125 11.854v-8.385H7.078v-3.47h3.047V9.43c0-3.007 1.792-4.669 4.533-4.669 1.312 0 2.686.235 2.686.235v2.953H15.83c-1.491 0-1.956.925-1.956 1.874v2.25h3.328l-.532 3.47h-2.796v8.385C19.612 23.027 24 18.062 24 12.073z"/></svg>&nbsp; <a href="http://facebook.com/<?PHP echo $_facebook; ?>" target="_blank"><?PHP if (strlen($_facebook) >= 17) { echo substr($_facebook,0,17).'&hellip;'; } else { echo $_facebook; } ?></a></p>
                              <div class="fb-follow" data-href="https://www.facebook.com/<?PHP echo $_facebook; ?>" data-layout="button" data-size="large" data-show-faces="false" data-height="350"></div>
                            </article>
<?PHP } ?>
                          </div>

                          <?PHP // MINI-CARTE ----------- ?>
                          <div class="tile is-parent is-vertical" style="padding: 0.75em 0 0 0;">
                            <article class="tile is-child notification" style="padding:0;border-radius:0px;margin:0px;">
                              <iframe width="auto" height="100%" src="maplabbzh_carte.php?mapbox=streets&lat=<?PHP echo $_latitude; ?>&lon=<?PHP echo $_longitude; ?>&type=<?PHP echo $_structure; ?>" name="Carte des FabLabs & Tiers-Lieux de Bretagne #MapLabBzh" style="width:100%;border:0px;"></iframe>
                            </article>
                          </div>
                        </div>

                        <?PHP // FLUX RSS ----------- ?>
                        <div class="tile is-parent" style="padding:0;">
                          <article class="tile is-child notification is-success" style="background-color: <?PHP if ($_rss != "") { echo '#f47721'; } else { echo '#CCC'; } ?>;border-radius:0px;margin:0px;">
                            <p class="title"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="#FFF" class="bi bi-rss-fill" viewBox="0 0 16 16">
  <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm1.5 2.5c5.523 0 10 4.477 10 10a1 1 0 1 1-2 0 8 8 0 0 0-8-8 1 1 0 0 1 0-2zm0 4a6 6 0 0 1 6 6 1 1 0 1 1-2 0 4 4 0 0 0-4-4 1 1 0 0 1 0-2zm.5 7a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z"/>
</svg>&nbsp; actualités</p>
                            <?PHP if ($_rss != "") { ?><p class="subtitle liens font-black" style="font-size:0.9em;"><a href="<?PHP echo $_rss; ?>" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M10 13a5 5 0 0 0 7.54.54l3-3a5 5 0 0 0-7.07-7.07l-1.72 1.71"></path><path d="M14 11a5 5 0 0 0-7.54-.54l-3 3a5 5 0 0 0 7.07 7.07l1.71-1.71"></path></svg>&nbsp; Permalien RSS/Atom</a></p>
                            <div class="content">
                              <?PHP getPosts($_rss); ?>
                            </div><?PHP } ?>
                          </article>
                        </div>
                      </div>

                      <div class="tile is-parent is-vertical" style="padding: 0.75em 0 0 0;">
                        <?PHP // A PROXIMITÉ ----------- ?>
<?PHP /*                        <article class="tile is-child notification is-dark" style="background-color:#8a7967;border-radius:0px;margin:0px;">
                          <p class="title"><span class="icon is-medium"><i class="fa fa-compass"></i></span> à proximité</p>
                        </article>
*/ ?>
                        <?PHP // DERNIER TWEET / POST FB ----------- ?>
<?PHP if ( ($_twitter != '') || ($_facebook != '') ) { // DEBUT ?>
                        <article class="tile is-child notification is-success" style="background-color:#c1d82f;border-radius:0px;margin:0px;">
                          <p class="title"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="#FFF" class="bi bi-phone-fill" viewBox="0 0 16 16">
  <path d="M3 2a2 2 0 0 1 2-2h6a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V2zm6 11a1 1 0 1 0-2 0 1 1 0 0 0 2 0z"/>
</svg>&nbsp; à suivre</p>
	<?PHP if ($_twitter != '') { ?>
                          <p class="content">
                              	<a class="twitter-timeline"
                                          href="https://twitter.com/<?PHP echo $_twitter; ?>"
                                          data-width="100%"
                                          data-height="300"
                                          data-chrome="nofooter noborders"
                                          data-tweet-limit="1"
                                          show-replies="false">
                                        Tweets by <?PHP echo $_twitter; ?>
                                        </a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

		     </p>
	<?PHP } ?>
	<?PHP if ($_facebook != '') { ?>
                         <p class="content">
                                   <div class="fb-page" data-href="https://www.facebook.com/<?PHP echo $_facebook; ?>" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/<?PHP echo $_facebook; ?>" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/<?PHP echo $_facebook; ?>"><?PHP echo $_nom; ?></a></blockquote></div>
                        </p>
	<?PHP } ?>
                        </article>
<?PHP } // FIN ?>
                        <?PHP // SIMILAIRES ----------- ?>
                        <article class="tile is-child notification is-danger" style="border-radius:0px;margin:0px;">
<?PHP
	// Description par types
	$tab_types = array('hakerspace'=>'hackerspaces', 'makerspace'=>'makerspaces', 'tierslieu'=>'tiers-lieux', 'usages'=>'lieux d\'usages', 'frenchtech'=>'French Tech', 'fablabMIT'=>'fabLabs labellisés', 'fablab'=>'fabLabs', 'cantine'=>'cantines & coworking', 'formation'=>'formations & recherche');
	$nb_similaires = count($tab_similaire);
?>
                            <p class="title liens"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="#FFF" class="bi bi-stack" viewBox="0 0 16 16">
  <path d="m14.12 10.163 1.715.858c.22.11.22.424 0 .534L8.267 15.34a.598.598 0 0 1-.534 0L.165 11.555a.299.299 0 0 1 0-.534l1.716-.858 5.317 2.659c.505.252 1.1.252 1.604 0l5.317-2.66zM7.733.063a.598.598 0 0 1 .534 0l7.568 3.784a.3.3 0 0 1 0 .535L8.267 8.165a.598.598 0 0 1-.534 0L.165 4.382a.299.299 0 0 1 0-.535L7.733.063z"/>
  <path d="m14.12 6.576 1.715.858c.22.11.22.424 0 .534l-7.568 3.784a.598.598 0 0 1-.534 0L.165 7.968a.299.299 0 0 1 0-.534l1.716-.858 5.317 2.659c.505.252 1.1.252 1.604 0l5.317-2.659z"/>
</svg>&nbsp; <?PHP echo $nb_similaires; ?> autres <?PHP echo $tab_types[$_structure]; ?></p>
                            <div class="content">
<?PHP
	for ($i=0; $i < $nb_similaires; $i++) {
		echo "\t".'<div style="margin-bottom:2px;">';
		echo '<a href="maplabbzh_fiche.php?id='.$tab_similaire[$i]['id'].'&type='.$_structure.'" target="_self" title="voir la fiche" class="font-black">'.$tab_similaire[$i]['name']. '</a> ('.$tab_similaire[$i]['ville'].')';
		echo '</div>'."\n";
	}
?>
                            </div>
                        </article>
                      </div>
                    </div>
	</section>

          <div id="page">

<?PHP


/*
	echo '<hr />';
	echo '<pre>';
		var_dump($tab_similaire);
	echo '</pre>';
*/

?>

	</div> <?PHP // FIN page ?>


	<?PHP 	// Menu top
	require_once ('maplabbzh_footer.inc.php');
	?>

</body>
</html>
