<?PHP

	/*	#MapLabBZH La carte des FabLabs & autres lieux de dissémination des usages numériques en Bretagne
		=================================================================================================
			Auteur	Guillaume Rouan | @grouan
			Blog	http://grouan.fr/
		-------------------------------------------------------------------------------------------------
			Titre	maplabbzh_liste.php
			Version	v.4 - juillet 2022
			Sources 	https://gitlab.com/grouan/maplabbzh/
			Licence	CC-BY
			Made with CSS Framework Bulma + Icons SVG https://iconsvg.xyz
		-------------------------------------------------------------------------------------------------
	*/

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Carte des FabLabs & Tiers-Lieux de dissémination des usages numériques en Bretagne #MapLabBZH - Guillaume Rouan - 2015 - CC BY-SA</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="css/bulma.css">
	<? $taille = 100; // taille des icones ?>
	<style type="text/css">
		#tuile {
			background-color:#EAEAEA;
			border-radius:0px;
			/*border: 10px solid #FFF;*/
		}
		#tuile:hover {
			background-color: #FFCC2F; /*jaune*/
		}
	</style>

	<script type="text/javascript">
		$(document).ready(function(){
			$('table.search-table').tableSearch({
				searchText:'<h1 class="title"><span class="icon is-medium"><i class="fa fa-filter"></i></span> Filtrez la liste</h1><h2 class="subtitle no-mobile">Utilisez le champ ci-dessous pour affiner votre recherche',
				searchPlaceHolder:'Que recherchez-vous ?'
			});
		});
	</script>

  <?PHP // Bulma Main JS Library ?>
	<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<section class="hero is-dark is-bold">
		<div class="hero-body">
			<div class="container">
				<h1 class="title">Carte des <span class="jaune">FabLabs</span> & <span class="jaune">Tiers-Lieux</span> de Bretagne</h1>
				<h2 class="subtitle">Bienvenue sur #MapLabBzh, la carte coopérative libre et open source des FabLabs, Tiers-Lieux & autres espaces de dissémination des usages numériques en Bretagne.</h2>
			</div>
		</div>
	</section>

	<section class="">

						<div class="tile is-ancestor">

							<div class="tile is-vertical is-8">

								<div class="tile">

									<div class="tile is-parent is-vertical has-text-centered">

										<a href="https://gitlab.com/grouan/maplabbzh/-/wikis/home" target="_blank">
										<article id="tuile" class="tile is-child notification is-warning">
											<div style="text-align:center;font-size:50px;"><svg xmlns="http://www.w3.org/2000/svg" width="<? echo $taille; ?>" height="<? echo $taille; ?>" viewBox="0 0 24 24" fill="none" stroke="#333" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M14 2H6a2 2 0 0 0-2 2v16c0 1.1.9 2 2 2h12a2 2 0 0 0 2-2V8l-6-6z"/><path d="M14 3v5h5M16 13H8M16 17H8M10 9H8"/></svg></div>
											<p class="title">Wiki</p>
											<p class="subtitle">PROJET</p>
										</article>
										</a>

										<a href="/maplabbzh/maplabbzh_liste" target="_self">
										<article id="tuile" class="tile is-child notification is-warning">
											<div style="text-align:center;"><svg xmlns="http://www.w3.org/2000/svg" width="<? echo $taille; ?>" height="<? echo $taille; ?>" viewBox="0 0 24 24" fill="none" stroke="#333" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="8" y1="6" x2="21" y2="6"></line><line x1="8" y1="12" x2="21" y2="12"></line><line x1="8" y1="18" x2="21" y2="18"></line><line x1="3" y1="6" x2="3.01" y2="6"></line><line x1="3" y1="12" x2="3.01" y2="12"></line><line x1="3" y1="18" x2="3.01" y2="18"></line></svg></div>
											<p class="title">Liste & recherche</p>
											<p class="subtitle">UI</p>
										</article>
										</a>

										<a href="https://gitlab.com/grouan/maplabbzh/-/blob/master/map_bzh_fablab.geojson" target="_blank">
										<article id="tuile" class="tile is-child notification is-warning">
											<div style="text-align:center;font-size:50px;"><svg xmlns="http://www.w3.org/2000/svg" width="<? echo $taille; ?>" height="<? echo $taille; ?>" viewBox="0 0 24 24" fill="none" stroke="#333" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg></div>
											<p class="title">Sources GeoJSON</p>
											<p class="subtitle">DATA</p>
										</article>
										</a>

									</div>

									<div class="tile is-parent is-vertical has-text-centered">

										<a href="/maplabbzh/maplabbzh_contribuer" target="_self">
										<article id="tuile" class="tile is-child notification is-warning">
											<div style="text-align:center;"><svg xmlns="http://www.w3.org/2000/svg" width="<? echo $taille; ?>" height="<? echo $taille; ?>" viewBox="0 0 24 24" fill="none" stroke="#333" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><line x1="20" y1="8" x2="20" y2="14"></line><line x1="23" y1="11" x2="17" y2="11"></line></svg></div>
											<p class="title">Contribution</p>
											<p class="subtitle">PROJET</p>
										</article>
										</a>

										<a href="/maplabbzh/maplabbzh_carte" target="_self">
										<article id="tuile" class="tile is-child notification is-warning">
											<div style="text-align:center;font-size:50px;"><svg xmlns="http://www.w3.org/2000/svg" width="<? echo $taille; ?>" height="<? echo $taille; ?>" viewBox="0 0 24 24" fill="none" stroke="#333" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="10" r="3"/><path d="M12 21.7C17.3 17 20 13 20 10a8 8 0 1 0-16 0c0 3 2.7 6.9 8 11.7z"/></svg></div>
											<p class="title">Carte</p>
											<p class="subtitle">UI</p>
										</article>
										</a>

										<a href="/maplabbzh/maplabbzh_csv" target="_self">
										<article id="tuile" class="tile is-child notification is-warning">
											<div style="text-align:center;font-size:50px;"><svg xmlns="http://www.w3.org/2000/svg" width="<? echo $taille; ?>" height="<? echo $taille; ?>" viewBox="0 0 24 24" fill="none" stroke="#333" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg></div>
											<p class="title">Export CSV</p>
											<p class="subtitle">DATA</p>
										</article>
										</a>

									</div>

								</div>

							</div>

							<div class="tile is-parent is-vertical has-text-centered">

								<a href="https://gitlab.com/grouan/maplabbzh" target="_blank">
								<article id="tuile" class="tile is-child notification is-warning">
									<div style="text-align:center;font-size:50px;"><svg role="img" width="<? echo $taille; ?>" height="<? echo $taille; ?>" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>GitLab</title><path d="m23.6004 9.5927-.0337-.0862L20.3.9814a.851.851 0 0 0-.3362-.405.8748.8748 0 0 0-.9997.0539.8748.8748 0 0 0-.29.4399l-2.2055 6.748H7.5375l-2.2057-6.748a.8573.8573 0 0 0-.29-.4412.8748.8748 0 0 0-.9997-.0537.8585.8585 0 0 0-.3362.4049L.4332 9.5015l-.0325.0862a6.0657 6.0657 0 0 0 2.0119 7.0105l.0113.0087.03.0213 4.976 3.7264 2.462 1.8633 1.4995 1.1321a1.0085 1.0085 0 0 0 1.2197 0l1.4995-1.1321 2.4619-1.8633 5.006-3.7489.0125-.01a6.0682 6.0682 0 0 0 2.0094-7.003z"/></svg></div>
									<p class="title">Dépôt</p>
									<p class="subtitle">PROJET</p>
								</article>
								</a>

								<a href="#" target="_self">
								<article id="tuile" class="tile is-child notification is-warning">
									<div style="text-align:center;"><svg xmlns="http://www.w3.org/2000/svg" width="<? echo $taille; ?>" height="<? echo $taille; ?>" viewBox="0 0 24 24" fill="none" stroke="#333" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="16 18 22 12 16 6"></polyline><polyline points="8 6 2 12 8 18"></polyline></svg></div>
									<p class="title">Intégration</p>
									<p class="subtitle">UI</p>
								</article>
								</a>

								<a href="/maplabbzh/maplabbzh_datalab" target="_self">
								<article id="tuile" class="tile is-child notification is-warning">
									<div style="text-align:center;font-size:50px;"><svg xmlns="http://www.w3.org/2000/svg" width="<? echo $taille; ?>" height="<? echo $taille; ?>" viewBox="0 0 24 24" fill="none" stroke="#333" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M3 3v18h18"/><path d="M18.7 8l-5.1 5.2-2.8-2.7L7 14.3"/></svg></div>
									<p class="title">Datalab</p>
									<p class="subtitle">DATA</p>
								</article>
								</a>

							</div>

						</div>

	</section>

	<section class="hero is-light is-bold">
		<div class="hero-body" style="margin:0;padding:0;padding-top:20px;background-color:#FFF;">
			<div class="container">
				<img src="img/maplabbzh_projet_light.png" alt="Diagramme du projet MapLabBzh montrant les liens entre les interfaces, les sources et le wiki" width="100%" />
			</div>
		</div>
	</section>

	<?PHP 	// Menu top
	require_once ('maplabbzh_footer.inc.php');
	?>

</body>
</html>
