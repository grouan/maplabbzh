<?PHP

	/*	#MapLabBZH La carte des FabLabs & autres lieux de dissémination des usages numériques en Bretagne
		=================================================================================================
			Auteur	Guillaume Rouan | @grouan
			Blog	https://grouan.fr/
		-------------------------------------------------------------------------------------------------
			Titre	maplabbzh_liste.php
			Version	v.4 - juillet 2022
			Sources 	https://gitlab.com/grouan/maplabbzh/
			Licence	CC-BY
			Made with CSS Framework Bulma + Font Awesome
		-------------------------------------------------------------------------------------------------
	*/

	// Variable de tri
	$tri = isset($_GET['tri']) ? $_GET['tri'] : 'name';

	// Sources : Github
	$json_source = file_get_contents('https://gitlab.com/grouan/maplabbzh/raw/master/map_bzh_fablab.geojson');

	//$json_data = json_decode($json_source); // Fichier brut
	$json_data = json_decode($json_source, true);  // Dans un tableau

	$nb_references = count ($json_data['features']); // nombre de références

	for ($i=0; $i < $nb_references; $i++) {

		// [ id | name | structure | adresse | cp | ville | web | twitter | facebook | email | tel | rss | longitude | latitude ]
		$tab_liste [$i]['id'] = $json_data['features'][$i]['properties']['id'];
		$tab_liste [$i]['name'] = $json_data['features'][$i]['properties']['name'];
		$tab_liste [$i]['structure'] = $json_data['features'][$i]['properties']['structure'];
		$tab_liste [$i]['adresse'] = $json_data['features'][$i]['properties']['adresse'];
		$tab_liste [$i]['cp'] = $json_data['features'][$i]['properties']['cp'];
		$tab_liste [$i]['ville'] = $json_data['features'][$i]['properties']['ville'];
		$tab_liste [$i]['web'] = $json_data['features'][$i]['properties']['web'];
		$tab_liste [$i]['twitter'] = $json_data['features'][$i]['properties']['twitter'];
		$tab_liste [$i]['facebook'] = $json_data['features'][$i]['properties']['facebook'];
		$tab_liste [$i]['email'] = $json_data['features'][$i]['properties']['email'];
		$tab_liste [$i]['tel'] = $json_data['features'][$i]['properties']['tel'];
		$tab_liste [$i]['rss'] = $json_data['features'][$i]['properties']['rss'];
		//
		$tab_liste [$i]['longitude'] = $json_data['features'][$i]['geometry']['coordinates'][0];
		$tab_liste [$i]['latitude'] = $json_data['features'][$i]['geometry']['coordinates'][1];

	}

	//$nb_colonnes = 11; // ATTENTION => "colspan" pour la minicarte !!

	// Tri
	foreach($tab_liste as $k => $v) { $tab_tri[$k] = $v[$tri]; }
	array_multisort($tab_tri, SORT_ASC, $tab_liste);


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Carte des FabLabs & Tiers-Lieux de dissémination des usages numériques en Bretagne #MapLabBZH - Guillaume Rouan - 2015 - CC BY-SA</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">

  <!--<link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">-->
	<link rel="stylesheet" href="css/bulma.css">

	<?PHP // jQuery Search : filtre / recherche dynamique ?>
	<script src='jquery/jquery-3.6.0.min.js'></script>
	<script src="jquery/html-table-search.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			$('table.search-table').tableSearch({
				searchText:'<h1 class="title"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="4" y1="21" x2="4" y2="14"></line><line x1="4" y1="10" x2="4" y2="3"></line><line x1="12" y1="21" x2="12" y2="12"></line><line x1="12" y1="8" x2="12" y2="3"></line><line x1="20" y1="21" x2="20" y2="16"></line><line x1="20" y1="12" x2="20" y2="3"></line><line x1="1" y1="14" x2="7" y2="14"></line><line x1="9" y1="8" x2="15" y2="8"></line><line x1="17" y1="16" x2="23" y2="16"></line></svg>&nbsp; Filtrez la liste</h1><h2 class="subtitle no-mobile">Utilisez le champ ci-dessous pour affiner votre recherche',
				searchPlaceHolder:'Que recherchez-vous ?'
			});
		});
	</script>

          <?PHP // Bulma Main JS Library ?>

	<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<section class="hero is-dark is-bold">


            <?PHP 	// Menu top
		require_once ('maplabbzh_menutop.inc.php');
	?>

		<div class="hero-body">
			<div class="container">
				<h1 class="title">Carte des <span class="jaune">FabLabs</span> & <span class="jaune">Tiers-Lieux</span> de Bretagne</h1>
				<h2 class="subtitle">Bienvenue sur #MapLabBzh, la carte coopérative libre et open source des FabLabs, Tiers-Lieux & autres espaces de dissémination des usages numériques en Bretagne. Les données ci-dessous sont synchronisées à partir du dépôt GitLab.</h2>
                                           <a class="button is-large">
																						 <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-map-fill" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M16 .5a.5.5 0 0 0-.598-.49L10.5.99 5.598.01a.5.5 0 0 0-.196 0l-5 1A.5.5 0 0 0 0 1.5v14a.5.5 0 0 0 .598.49l4.902-.98 4.902.98a.502.502 0 0 0 .196 0l5-1A.5.5 0 0 0 16 14.5V.5zM5 14.09V1.11l.5-.1.5.1v12.98l-.402-.08a.498.498 0 0 0-.196 0L5 14.09zm5 .8V1.91l.402.08a.5.5 0 0 0 .196 0L11 1.91v12.98l-.5.1.5-.1z"/></svg>&nbsp;
                                            <span class="font-bold"><?PHP echo $nb_references; ?> références</span>
                                          </a>
                                          <a class="button is-success is-large" href="maplabbzh_contribuer.php" alt="" title="Ajoutez / Modifiez / Supprimez / Suggérez des infos">
                              	    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><line x1="20" y1="8" x2="20" y2="14"></line><line x1="23" y1="11" x2="17" y2="11"></line></svg>&nbsp;
				    <span class="font-bold">Contribuez !</span>
				  </a>
			</div>
		</div>
	</section>

  <section class="section" style="padding:0px;">
      <?PHP // FIN header

	// [ id | name | structure | adresse | cp | ville | web | twitter | facebook | email | tel | rss | longitude | latitude ]
	echo "\t".'<table cellpading="0" cellspacing="0" class="search-table table is-narrow is-fullwidth">'."\n";
	echo "\t".'<thead>'."\n";
		echo "\t\t".'<tr>'."\n";
			echo "\t\t\t".'<td class="titre">&nbsp;</td>'."\n";
			echo "\t\t\t".'<td class="titre"><a href="?tri=name" target="_self" title="trier par noms"><span class="icon"><i class="fa fa-sort"></i></span><span class="font-black"> nom</span></a></td>'."\n";
			echo "\t\t\t".'<td class="titre" width="70px;"><a href="?tri=structure" target="_self" title="trier par types"><span class="icon"><i class="fa fa-sort"></i></span><span class="font-black"> type</span></a></td>'."\n";
			echo "\t\t\t".'<td class="titre no-mobile">adresse</td>'."\n";
			echo "\t\t\t".'<td class="titre no-mobile"><a href="?tri=cp" target="_self" title="trier par codes postaux"><span class="icon"><i class="fa fa-sort"></i></span><span class="font-black"> cp</span></a></td>'."\n";
			echo "\t\t\t".'<td class="titre"><a href="?tri=ville" target="_self" title="trier par villes"><span class="icon"><i class="fa fa-sort"></i></span><span class="font-black"> ville</span></a></td>'."\n";
			echo "\t\t\t".'<td class="titre no-mobile"><i class="fa fa-globe fa-lg" aria-hidden="true"></i></td>'."\n";
			echo "\t\t\t".'<td class="titre no-mobile"><i class="fa fa-twitter fa-lg" aria-hidden="true"></i></td>'."\n";
			echo "\t\t\t".'<td class="titre no-mobile"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></td>'."\n";
			echo "\t\t\t".'<td class="titre no-mobile"><i class="fa fa-envelope fa-lg" aria-hidden="true"></i></td>'."\n";
			echo "\t\t\t".'<td class="titre no-mobile"><i class="fa fa-rss fa-lg" aria-hidden="true"></i></td>'."\n";
			echo "\t\t\t".'<td class="titre no-mobile"><i class="fa fa-map-marker fa-lg" aria-hidden="true"></i></td>'."\n";
		echo "\t\t".'</tr>'."\n";
	echo "\t".'</thead>'."\n";
		echo "\t".'<tbody>'."\n";

	// Affichage
	for ($j=0; $j<count($tab_liste); $j++) {

		echo "\t\t".'<tr>'."\n";
			echo "\t\t\t".'<td class="fiche"><a href="/maplabbzh/maplabbzh_fiche.php?id='.$tab_liste[$j]['id'].'&type='.$tab_liste[$j]['structure'].'" target="_self" title="voir la fiche"><i class="fa fa-id-card fa-lg" aria-hidden="true"></i></a></td>'."\n";
			echo "\t\t\t".'<td class="name font-bold"><a href="/maplabbzh/maplabbzh_fiche.php?id='.$tab_liste[$j]['id'].'&type='.$tab_liste[$j]['structure'].'" target="_self" title="voir la fiche">'.$tab_liste[$j]['name'].'<a></td>'."\n";

				// Paramétrage des marqueurs (en fonction du type de structure)
				if ($tab_liste[$j]['structure'] == 'usages') {
					$title_structure = 'Espace ou organisation facilitant ou développant les usages numériques'; $img_structure = 'https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_usages.png';
				}
				if ($tab_liste[$j]['structure'] == 'fablabMIT') {
					$title_structure = 'FabLab affilié à la Fab Foundation du MIT'; $img_structure = 'https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_fablabMIT.png';
				}
				if ($tab_liste[$j]['structure'] == 'cantine') {
					$title_structure = 'Cantine numérique ou espace de coworking'; $img_structure = 'https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_cantine.png';
				}
				if ($tab_liste[$j]['structure'] == 'hakerspace') {
					$title_structure = 'Hackerspace'; $img_structure = 'https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_hakerspace.png';
				}
				if ($tab_liste[$j]['structure'] == 'fablab') {
					$title_structure = 'FabLab (Laboratoire de Fabrication Numérique)'; $img_structure = 'https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_fablab.png';
				}
				if ($tab_liste[$j]['structure'] == 'makerspace') {
					$title_structure = 'Makerspace'; $img_structure = 'https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_makerspace.png';
				}
				if ($tab_liste[$j]['structure'] == 'tierslieu') {
					$title_structure = 'Tiers-Lieu'; $img_structure = 'https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_tierslieu.png';
				}
				if ($tab_liste[$j]['structure'] == 'formation') {
					$title_structure = 'Formation / Recherche'; $img_structure = 'https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_formation.png';
				}
				if ($tab_liste[$j]['structure'] == 'frenchtech') {
					$title_structure = 'FrenchTech'; $img_structure = 'https://gitlab.com/grouan/maplabbzh/raw/master/img/osm_marker_frenchtech.png';
				}

			echo "\t\t\t".'<td><a href="/maplabbzh/maplabbzh_fiche.php?id='.$tab_liste[$j]['id'].'&type='.$tab_liste[$j]['structure'].'" target="_self" title="voir la fiche"><img src="'.$img_structure.'" title="'.$title_structure.'" /></a></td>'."\n";
			echo "\t\t\t".'<td class="no-mobile">'.$tab_liste[$j]['adresse'].'</td>'."\n";
			echo "\t\t\t".'<td class="no-mobile">'.$tab_liste[$j]['cp'].'</td>'."\n";
			echo "\t\t\t".'<td>'.$tab_liste[$j]['ville'].'</td>'."\n";
			echo "\t\t\t".'<td class="no-mobile">';
				if (!empty($tab_liste[$j]['web'])) { echo '<a href="'.$tab_liste[$j]['web'].'" target="_blank" title="&rarr; '.$tab_liste[$j]['web'].'"><i class="fa fa-globe fa-lg" aria-hidden="true"></i></a>'; }
				else { echo '<span class="gris">—</span>'; }
			echo '</td>'."\n";
			echo "\t\t\t".'<td class="no-mobile">';
				if (!empty($tab_liste[$j]['twitter'])) { echo '<a href="https://twitter.com/'.$tab_liste[$j]['twitter'].'" target="_blank" title="&rarr; '.$tab_liste[$j]['twitter'].'"><i class="fa fa-twitter fa-lg" aria-hidden="true"></i></a>'; }
				else { echo '<span class="gris">—</span>'; }
			echo '</td>'."\n";
			echo "\t\t\t".'<td class="no-mobile">';
				if (!empty($tab_liste[$j]['facebook'])) { echo '<a href="https://facebook.com/'.$tab_liste[$j]['facebook'].'" target="_blank" title="&rarr; '.$tab_liste[$j]['facebook'].'"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a>'; }
				else { echo '<span class="gris">—</span>'; }
			echo '</td>'."\n";
			echo "\t\t\t".'<td class="no-mobile">';
				if (!empty($tab_liste[$j]['email'])) { echo '<a href="mailto:'.$tab_liste[$j]['email'].'?subject=#MapLabBZH" target="_blank" title="Contacter par courriel"><i class="fa fa-envelope fa-lg" aria-hidden="true"></i></a>'; }
				else { echo '<span class="gris">—</span>'; }
			echo '</td>'."\n";
			echo "\t\t\t".'<td class="no-mobile">';
				if (!empty($tab_liste[$j]['rss'])) { echo '<a href="'.$tab_liste[$j]['rss'].'" target="_blank" title="&rarr; Suivre les actualités"><i class="fa fa-rss fa-lg" aria-hidden="true"></i></a>'; }
				else { echo '<span class="gris">—</span>'; }
			echo '</td>'."\n";
			echo "\t\t\t".'<td class="longlat no-mobile">'.number_format($tab_liste[$j]['latitude'],4).', '.number_format($tab_liste[$j]['longitude'],4).'</td>'."\n";
		echo "\t\t".'</tr>'."\n";

		/*
		echo '<tr>'; // Mini-carte
			echo '<td colspan="'.$nb_colonnes.'" style="padding:0px;border:0px;"><div id="minimap'.$tab_liste[$j]['id'].'" style="display:none;">';
				echo '<iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox='.$tab_liste[$j]['longitude'].'%2C'.$tab_liste[$j]['latitude'].'%2C'.$tab_liste[$j]['longitude'].'%2C'.$tab_liste[$j]['latitude'].'&amp;layer=mapnik&amp;marker='.$tab_liste[$j]['latitude'].'%2C'.$tab_liste[$j]['longitude'].'"></iframe>';
			echo '</div></td>';
		echo '</tr>';
		*/
	}

	echo "\t".'</tbody>'."\n";
	echo "\t".'</table>'."\n";

	?>
  </section>

  <section class="section" style="padding:0px;border-top:8px solid #555;">
  	<iframe width="100%" height="400px" src="https://grouan.fr/maplabbzh/maplabbzh_carte.php?mapbox=dark" name="Carte des FabLabs & Tiers-Lieux de Bretagne" style="width:100%;border:0px;"></iframe>
  </section>

	<?PHP 	// Menu top
require_once ('maplabbzh_footer.inc.php');
?>

</body>
</html>
