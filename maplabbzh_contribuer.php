<?PHP

/*	#MapLabBZH La carte des FabLabs & Tiers Lieux de dissémination des usages numériques en Bretagne
	=================================================================================================
		Auteur	Guillaume Rouan | @grouan
		Blog	https://grouan.fr
	-------------------------------------------------------------------------------------------------
		Titre	= maplabbzh_contribuer.php
		Sources = https://gitlab.com/grouan/maplabbzh
		Licence	= CC BY-SA pour les contenus / EUPL pour le code source
		UI Liste = CSS Framework Bulma + Font Awesome
		UI Cartographie = Mapbox + OpenStreetMap
		Datas GeoJSON = Guillaume Rouan
		Création : octobre 2015 | v.4 juillet 2022
	-------------------------------------------------------------------------------------------------
*/

	// Récupération d'infos de connexion
	$adresse_ip = $_SERVER['REMOTE_ADDR']; // REMOTE ADDR = adresse IP
	$navigateur = $_SERVER['HTTP_USER_AGENT']; // HTTP USER AGENT = navigateur

	// Types de pôles
	$tt = array ('fablab','fablabMIT','makerspace','hakerspace','tierslieu','usages','formation','cantine','frenchtech');
	$tn = array ('FabLab / Atelier de fabrication','FabLab labellisé par la Fab Foundation','Makerspace','Hackerspace','Tiers-Lieu','Espace / Organisation facilitant les usages numériques','Formation / Recherche','Cantine numérique / Espace de coworking','French Tech');


if (isset($_GET['id'])) { // Contribuer à 1 fiche précise

	// Sources JSON : GitLab
	$json_source = file_get_contents('https://gitlab.com/grouan/maplabbzh/raw/master/map_bzh_fablab.geojson');

	//$json_data = json_decode($json_source); // Fichier brut
	$json_data = json_decode($json_source, true);  // Dans un tableau

	// Recherche de l'identifiant dans le fichier JSON
	$nb_references = count ($json_data['features']);
	for ($i=0; $i < $nb_references; $i++) {

		if ($json_data['features'][$i]['properties']['id'] == $_GET['id']) {
			$_nom 		= $json_data['features'][$i]['properties']['name'];
			$_structure 	= $json_data['features'][$i]['properties']['structure'];
			$_adresse 	= $json_data['features'][$i]['properties']['adresse'];
			$_cp 		= $json_data['features'][$i]['properties']['cp'];
			$_ville 		= $json_data['features'][$i]['properties']['ville'];
			$_web 		= $json_data['features'][$i]['properties']['web'];
			$_twitter 	= $json_data['features'][$i]['properties']['twitter'];
			$_facebook 	= $json_data['features'][$i]['properties']['facebook'];
			$_email 		= $json_data['features'][$i]['properties']['email'];
			$_tel 		= $json_data['features'][$i]['properties']['tel'];
			$_rss 		= $json_data['features'][$i]['properties']['rss'];
			$_longitude 	= $json_data['features'][$i]['geometry']['coordinates'][0];
			$_latitude 	= $json_data['features'][$i]['geometry']['coordinates'][1];
		}

  	}

}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Carte des FabLabs & Tiers-Lieux de Bretagne #MapLabBZH</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">

          <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bulma.css">

          <?PHP // Bulma Main JS Library ?>
	<script type="text/javascript" src="js/main.js"></script>


</head>

<body>

          <section class="hero is-dark is-bold">
	<?PHP require_once ('maplabbzh_menutop.inc.php'); // Menu Top ?>
          </section>

          <?PHP // BREADCRUMB ----------- ?>
          <section>
          <nav class="breadcrumb is-medium is-right" aria-label="breadcrumbs"> <?PHP // Accueil Liste ?>
            <ul class="font-black" style="background-color:#FFCC2F;color:#FFF;">
              <li><a href="maplabbzh_liste.php" target="_self" style="color:#FFF;"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M19 12H6M12 5l-7 7 7 7"/></svg> &nbsp; <span>Retour à la liste</span></a></li>
            <?PHP if (isset($_GET['id'])) { // Fiche id ?>
              <li><a href="maplabbzh_fiche.php<?PHP if (isset($_GET['id'])) { echo '?id='.$_GET['id'].'&type='.$_structure; } ?>" target="_self" style="color:#FFF;"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="10"></circle><circle cx="12" cy="12" r="3"></circle></svg>&nbsp; <span><?PHP echo $_nom; ?></span></a></li>
            <?PHP } ?>
	  <?PHP if (isset($_GET['cat'])) { // Accueil Contributions ?>
              <li><a href="maplabbzh_contribuer.php<?PHP if (isset($_GET['id'])) { echo '?id='.$_GET['id']; } ?>" target="_self" style="color:#FFF;"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><line x1="20" y1="8" x2="20" y2="14"></line><line x1="23" y1="11" x2="17" y2="11"></line></svg> &nbsp; <span>Contribuez</span></a></li>
            <?PHP } ?>
            </ul>
          </nav>
          </section>

          <?PHP // NOM ----------- ?>
          <section class="hero is-light">
            <div class="hero-body">
              <div class="container">
                <h1 class="title" style="font-size:3em;text-transform:uppercase;"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24" fill="none" stroke="#333" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><line x1="20" y1="8" x2="20" y2="14"></line><line x1="23" y1="11" x2="17" y2="11"></line></svg> <span class="font-bold">
                <?PHP
                	if (isset($_GET['cat'])) { // Accueil Contributions
	      		if ($_GET['cat'] == 'ajouter_new') { echo 'Ajouter un pôle'; }
			if ($_GET['cat'] == 'ajouter_infos') { echo 'Ajouter des infos'; }
			if ($_GET['cat'] == 'supprimer') { echo 'Supprimer un pôle'; }
			if ($_GET['cat'] == 'modifier') { echo 'Modifier un pôle'; }
			if ($_GET['cat'] == 'suggerer') { echo 'Suggérer une amélioration'; }
			if ($_GET['cat'] == 'autre') { echo 'Autre&hellip;'; }
		} else {
			echo 'Contribuez';
		}
	      ?>
                </span></h1>

                <h2 class="subtitle no-mobile">
                  Votre contribution est essentielle à la mise-à-jour de cette carte ainsi qu'à sa qualité et à sa pérennité. Afin de garantir son intégrité et sa pertinence, je mets à jour le fichier source après quelques vérifications d'usage, dans le respect de votre proposition et dans les meilleurs délais. Vos contributions ne sont donc pas automatiquement actualisées. La plupart du temps je vous informe de la prise en compte effective de vos infos via les réseaux sociaux 😀
                </h2>
              </div>
            </div>
          </section>
	<?PHP if ( (isset($_GET['id'])) && (isset($_GET['cat'])) ) { ?>
	<section class="hero is-danger">
            <div class="hero-body">
              <div class="container">
                <h1 class="title">
                  <?PHP echo $_nom; ?>
                </h1>
              </div>
            </div>
          </section>
          <?PHP } else { ?>
          <section style="border-bottom:8px solid #ff3860;margin-top:-25px;">&nbsp;</section>
          <?PHP } ?>

<?PHP if (!isset($_GET['cat'])) { // Accueil ?>

          <section class="">

                    <div class="tile is-ancestor">

                      <div class="tile is-vertical is-8">

                        <div class="tile">

                          <div class="tile is-parent is-vertical has-text-centered">

                            <a href="maplabbzh_contribuer?cat=ajouter_new" target="_self">
                            <article class="tile is-child notification is-warning" style="background-color:#C1D82F;border-radius:0px;">
                              <div style="text-align:center;"><img src="icons/ico_150x150_panneau.png" style="width:96px;" /></div>
                              <p class="title">Ajouter</p>
                              <p class="subtitle">un nouveau pôle</p>
                            </article>
                            </a>

                            <a href="maplabbzh_contribuer?cat=supprimer<?PHP if (isset($_GET['id'])) { echo '&id='.$_GET['id']; } ?>" target="_self">
                            <article class="tile is-child notification is-primary" style="background-color:#CE1126;border-radius:0px;">
                              <div style="text-align:center;"><img src="icons/ico_150x150_pointillets.png" style="width:96px;" /></div>
                              <p class="title">Supprimer</p>
                              <p class="subtitle"><?PHP if (isset($_GET['id'])) { echo '<span class="font-bold">'.$_nom.'</span>'; } else { echo 'un pôle'; } ?></p>
                            </article>
                            </a>

                          </div>

                          <div class="tile is-parent is-vertical has-text-centered">

                            <a href="maplabbzh_contribuer?cat=ajouter_infos<?PHP if (isset($_GET['id'])) { echo '&id='.$_GET['id']; } ?>" target="_self">
                            <article class="tile is-child notification is-warning" style="background-color:#FBB034;border-radius:0px;">
                              <div style="text-align:center;"><img src="icons/ico_150x150_carte.png" style="width:96px;" /></div>
                              <p class="title">Ajouter</p>
                              <p class="subtitle">des infos <?PHP if (isset($_GET['id'])) { echo 'à <span class="font-bold">'.$_nom.'</span>'; } ?></p>
                            </article>
                            </a>

                            <a href="maplabbzh_contribuer?cat=suggerer" target="_self">
                            <article class="tile is-child notification is-warning" style="background-color:#1CC7D0;border-radius:0px;">
                              <div style="text-align:center;"><img src="icons/ico_150x150_drapeau.png" style="width:96px;" /></div>
                              <p class="title">Suggérer</p>
                              <p class="subtitle">une amélioration</p>
                            </article>
                            </a>

                          </div>

                        </div>

                      </div>

                      <div class="tile is-parent is-vertical has-text-centered">

                        <a href="maplabbzh_contribuer?cat=modifier<?PHP if (isset($_GET['id'])) { echo '&id='.$_GET['id']; } ?>" target="_self">
                            <article class="tile is-child notification is-warning" style="background-color:#EA4C89;border-radius:0px;">
                              <div style="text-align:center;"><img src="icons/ico_150x150_geoloc.png" style="width:96px;" /></div>
                              <p class="title">Modifier</p>
                              <p class="subtitle"><?PHP if (isset($_GET['id'])) { echo '<span class="font-bold">'.$_nom.'</span>'; } else { echo 'une/des info(s)'; } ?></p>
                            </article>
                            </a>

                            <a href="maplabbzh_contribuer?cat=autre" target="_self">
                            <article class="tile is-child notification is-primary" style="background-color:#C68143;border-radius:0px;">
                              <div style="text-align:center;"><img src="icons/ico_150x150_boussole.png" style="width:96px;" /></div>
                              <p class="title">Autre</p>
                              <p class="subtitle">&hellip;</p>
                            </article>
                            </a>

                      </div>

                    </div>

          </section>

<?PHP } else { // Rubrique des contributions ?>

<?PHP if ($_GET['cat'] == 'ajouter_new') { // Créer une nouvelle fiche  —————————————————————————————————————————————————————————— ?>

          <FORM action="maplabbzh_submit.php?cat=ajouter_new" method="post">

          <section class="hero is-light">
            <div class="hero-body">
              <div class="container">
                <h1 class="title">
                  Que souhaitez-vous ajouter ?
                </h1>

                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Nom du pôle" name="_nom" /></div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Organisme gestionnaire" name="_organisme" /></div>
                </div>
                <div class="control">
                  <div class="field">
                    <label class="label font-bold">Type de pôle</label>
                    <div class="control has-icons-left">
                      <div class="select">
                        <select id="_type">
                        <?PHP

		      for ($i=0; $i<count($tt); $i++) {
			$option = '<option value="'.$tt[$i].'">'.$tn[$i].'</option>'."\n";
			echo $option;
		      }

                        ?>
		    </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="field">
                  <br /><label class="label font-bold">Adresse</label>
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Adresse" name="_adresse" />
                    </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Code postal" name="_cp" maxlength="5" />
                    </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Ville" name="_ville" />
                    </div>
                </div>
                <div class="field">
                  <label class="label font-bold">Contact</label>
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Courriel" name="_email" />
                    </div>
                </div>
                <div class="field">
                  <label class="label font-bold">Web & réseaux sociaux</label>
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Site web" name="_web" />
                    </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Flux RSS du site web" name="_rss" />
                    </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Page Facebook" name="_facebook" />
                    </div>
                </div>
                <div class="field">
                    <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Profil Twitter" name="_twitter" />
                    </div>
                </div>
                <div class="field">
                  <label class="label font-bold">Géolocalisation</label>
                  <p class="help is-dark">Indiquez une localisation aussi précise que possible, idéalement au bâtiment près. N'indiquez pas une géolocalisation sur la ville. Si vous avez un doute, je m'en chargerai :)</p>
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Longitude (ex: -2.345678)" name="_GPSlon" />
                    </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Latitude (ex: 23.456789)" name="_GPSlat" />
                    </div>
                </div>

              </div>
            </div>

          </section>

          <section class="hero is-warning" style="background-color:#4dc9f6;">
            <div class="hero-body">
              <div class="container">
                <h1 class="title">
                  Qui êtes-vous ?
                </h1>

                <div class="field">
                  <?PHP // <label class="label">Nom / Prénom</label> ?>
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Nom / Prénom" name="contact_nom" /></div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Statut" name="contact_statut" /></div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Courriel" name="contact_email" /></div>
                </div>
                <div class="field">
                  <div class="control">
                    <textarea class="textarea" type="text" placeholder="Un message ?" name="contact_message"></textarea>
                  </div>
                </div>

                <input type="hidden" value="<?PHP echo $adresse_ip; ?>" name="adresse_ip" />
                <input type="hidden" value="<?PHP echo $navigateur; ?>" name="navigateur" />

              </div>
            </div>
          </section>

          <section>
          <div class="field" style="margin:30px 0px;">
              <div class="field is-grouped is-grouped-centered">
                <p class="control" id="bouton_envoyer">
                    <button type="submit" class="button is-success font-bold"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="20 6 9 17 4 12"></polyline></svg> &nbsp; <span>Envoyer</span></button>
                <p class="control" id="bouton_annuler">
                    <button type="reset" class="button is-danger font-bold"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="10"></circle><line x1="4.93" y1="4.93" x2="19.07" y2="19.07"></line></svg> &nbsp;<span>Annuler</span></button>
              </div>
            </div>
          </section>

          </FORM>



<?PHP } if ($_GET['cat'] == 'ajouter_infos') { // Ajouter des infos complémentaires à une fiche [id] ————————————————————————————————— ?>

          <?PHP if (isset($_GET['id'])) { // Infos supplémentaires pour 1 pôle ?>
          <FORM action="maplabbzh_submit.php?cat=ajouter_infos" method="post">

          <section class="hero is-light">
            <div class="hero-body">
              <div class="container">
                <h1 class="title">
                  Que souhaitez-vous ajouter ?
                </h1>

                <div class="field">
                  <div class="control has-icons-left">
                    <?PHP if ($_nom!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_nom; ?>" value="" name="_nom" disabled />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Nom du pôle" name="_nom" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" name="_organisme" placeholder="Organisme gestionnaire" /></div>
                </div>
                <div class="control">
                  <div class="field">
                    <label class="label font-bold">Type de pôle</label>
                    <div class="control has-icons-left">
                      <input class="input is-warning font-bold" type="text" name="_type" placeholder="<?PHP echo $_structure; ?>" value="" disabled />

                    </div>
                  </div>
                </div>
                <div class="field">
                  <br /><label class="label font-bold">Adresse</label>
                  <div class="control has-icons-left">
                    <?PHP if ($_adresse!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_adresse; ?>" value="" name="_adresse" disabled />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Adresse" name="_adresse" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <?PHP if ($_cp!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_cp; ?>" value="" name="_cp" disabled />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Code postal" name="_cp" maxlength="5" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <?PHP if ($_ville!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_ville; ?>" value="" name="_ville" disabled />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Ville" name="_ville" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                  <label class="label font-bold">Contact</label>
                  <div class="control has-icons-left">
                    <?PHP if ($_email!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_email; ?>" value="" name="_email" disabled />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Courriel" name="_email" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                  <label class="label font-bold">Web & réseaux sociaux</label>
                  <div class="control has-icons-left">
                    <?PHP if ($_web!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_web; ?>" value="" name="_web" disabled />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Site web" name="_web" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <?PHP if ($_rss!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_rss; ?>" value="" name="_rss" disabled />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Flux RSS du site web" name="_rss" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <?PHP if ($_facebook!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_facebook; ?>" value="" name="_facebook" disabled />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Page Facebook" name="_facebook" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                    <div class="control has-icons-left">
                    <?PHP if ($_twitter!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_twitter; ?>" value="" name="_twitter" disabled />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Profil Twitter" name="_twitter" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                  <label class="label font-bold">Géolocalisation</label>
                  <?PHP if ($_longitude=='') { ?>
                  <p class="help is-dark">Indiquez une localisation aussi précise que possible, idéalement au bâtiment près. N'indiquez pas une géolocalisation sur la ville. Si vous avez un doute, je m'en chargerai :)</p>
                  <?PHP } ?>
                  <div class="control has-icons-left">
                    <?PHP if ($_longitude!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_longitude; ?>" value="" name="_GPSlon" disabled />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Longitude (ex: -2.345678)" name="_GPSlon" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <?PHP if ($_latitude!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_latitude; ?>" value="" name="_GPSlat" disabled />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Latitude (ex: 23.456789)" name="_GPSlat" />
                    <?PHP } ?>
                    </div>
                </div>

              </div>

            </div>

          </section>

          <section class="hero is-warning" style="background-color:#4dc9f6;">
            <div class="hero-body">
              <div class="container">
                <h1 class="title">
                  Qui êtes-vous ?
                </h1>

                <div class="field">
                  <?PHP // <label class="label">Nom / Prénom</label> ?>
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Nom / Prénom" name="contact_nom" /></div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Statut" name="contact_statut" /></div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Courriel" name="contact_email" /></div>
                </div>
                <div class="field">
                  <div class="control">
                    <textarea class="textarea" type="text" placeholder="Un message ?" name="contact_message"></textarea>
                  </div>
                </div>

                <input type="hidden" value="<?PHP echo $adresse_ip; ?>" name="adresse_ip" />
                <input type="hidden" value="<?PHP echo $navigateur; ?>" name="navigateur" />
                <input type="hidden" value="<?PHP echo $_GET['id']; ?>" name="identifiant" />

              </div>
            </div>
          </section>

          <section>
          <div class="field" style="margin:30px 0px;">
              <div class="field is-grouped is-grouped-centered">
                <p class="control" id="bouton_envoyer">
                    <button type="submit" class="button is-success font-bold"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="20 6 9 17 4 12"></polyline></svg> &nbsp; <span>Envoyer</span></button>
                <p class="control" id="bouton_annuler">
                    <button type="reset" class="button is-danger font-bold"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="10"></circle><line x1="4.93" y1="4.93" x2="19.07" y2="19.07"></line></svg> &nbsp;<span>Annuler</span></button>
              </div>
            </div>
          </section>

          </FORM>
          <?PHP } else { // Pas de pôle sélectionné => message erreur ?>
          <section class="hero is-danger">
            <div class="hero-body">
              <div class="container">
                <h1 class="title">
                  Sélectionnez un pôle
                </h1>
                <h2 class="subtitle">
                  Pour ajouter des informations complémentaires, <span class="font-bold"><a href="maplabbzh_liste.php" target="_self">veuillez sélectionner un pôle</a></span> puis cliquer sur le bouton <i>"Améliorez cette fiche"</i>
                </h2>
              </div>
            </div>
          </section>
          <?PHP } ?>

<?PHP } if ($_GET['cat'] == 'supprimer') { // Supprimer une fiche [id] —————————————————————————————————————————————————————————— ?>

          <?PHP if (isset($_GET['id'])) { // Supprimer 1 pôle ?>
          <FORM action="maplabbzh_submit.php?cat=supprimer" method="post">
          <section class="hero is-light">
            <div class="hero-body">
              <div class="container">
                <h1 class="title">
                  Pourquoi souhaitez-vous supprimer ce pôle ?
                </h1>
                <div class="field">
                  <div class="control has-icons-left">
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_nom; ?>" value="<?PHP echo $_nom; ?>" name="_nom" disabled />
                    </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_structure; ?>" value="<?PHP echo $_structure; ?>" name="_type" disabled />
                    </div>
                </div>

                <div class="field">
                  <div class="control">
                    <textarea class="textarea" type="text" placeholder="Votre message" name="contact_message"></textarea>
                  </div>
                </div>

              </div>
            </div>

          </section>

          <section class="hero is-warning" style="background-color:#4dc9f6;">
            <div class="hero-body">
              <div class="container">
                <h1 class="title">
                  Qui êtes-vous ?
                </h1>

                <div class="field">
                  <?PHP // <label class="label">Nom / Prénom</label> ?>
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Nom / Prénom" name="contact_nom" /></div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Statut" name="contact_statut" /></div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Courriel" name="contact_email" /></div>
                </div>

                <input type="hidden" value="<?PHP echo $adresse_ip; ?>" name="adresse_ip" />
                <input type="hidden" value="<?PHP echo $navigateur; ?>" name="navigateur" />
                <input type="hidden" value="<?PHP echo $_GET['id']; ?>" name="identifiant" />

              </div>
            </div>
          </section>

          <section>
          <div class="field" style="margin:30px 0px;">
              <div class="field is-grouped is-grouped-centered">
                <p class="control" id="bouton_envoyer">
                    <button type="submit" class="button is-success font-bold"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="20 6 9 17 4 12"></polyline></svg> &nbsp; <span>Envoyer</span></button>
                <p class="control" id="bouton_annuler">
                    <button type="reset" class="button is-danger font-bold"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="10"></circle><line x1="4.93" y1="4.93" x2="19.07" y2="19.07"></line></svg> &nbsp;<span>Annuler</span></button>
              </div>
            </div>
          </section>
          </FORM>
          <?PHP } else { // Pas de pôle sélectionné => message erreur ?>
          <section class="hero is-danger">
            <div class="hero-body">
              <div class="container">
                <h1 class="title">
                  Sélectionnez un pôle
                </h1>
                <h2 class="subtitle">
                  Pour supprimer un pôle, <span class="font-bold"><a href="maplabbzh_liste.php" target="_self">veuillez en sélectionner un</a></span> puis cliquer sur le bouton <i>"Améliorez cette fiche"</i>
                </h2>
              </div>
            </div>
          </section>
          <?PHP } ?>

<?PHP } if ($_GET['cat'] == 'modifier') { // Modifier une fiche [id] ——————————————————————————————————————————————————————————————— ?>

          <?PHP if (isset($_GET['id'])) { // Infos supplémentaires pour 1 pôle ?>
          <FORM action="maplabbzh_submit.php?cat=modifier" method="post">

          <section class="hero is-light">
            <div class="hero-body">
              <div class="container">
                <h1 class="title">
                  Que souhaitez-vous modifier ?
                </h1>

                <div class="field">
                  <div class="control has-icons-left">
                    <?PHP if ($_nom!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_nom; ?>" value="<?PHP echo $_nom; ?>" name="_nom" />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Nom du pôle" name="_nom" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" name="_organisme" placeholder="Organisme gestionnaire" /></div>
                </div>
                <div class="control">
                  <div class="field">
                    <label class="label font-bold">Type de pôle</label>
                    <div class="control has-icons-left">
                      <input class="input is-warning font-bold" type="text" name="_type" placeholder="<?PHP echo $_structure; ?>" value="<?PHP echo $_structure; ?>" />

                    </div>
                  </div>
                </div>
                <div class="field">
                  <br /><label class="label font-bold">Adresse</label>
                  <div class="control has-icons-left">
                    <?PHP if ($_adresse!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_adresse; ?>" value="<?PHP echo $_adresse; ?>" name="_adresse" />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Adresse" name="_adresse" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <?PHP if ($_cp!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_cp; ?>" value="<?PHP echo $_cp; ?>" name="_cp" />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Code postal" name="_cp" maxlength="5" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <?PHP if ($_ville!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_ville; ?>" value="<?PHP echo $_ville; ?>" name="_ville" />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Ville" name="_ville" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                  <label class="label font-bold">Contact</label>
                  <div class="control has-icons-left">
                    <?PHP if ($_email!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_email; ?>" value="<?PHP echo $_email; ?>" name="_email" />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Courriel" name="_email" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                  <label class="label font-bold">Web & réseaux sociaux</label>
                  <div class="control has-icons-left">
                    <?PHP if ($_web!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_web; ?>" value="<?PHP echo $_web; ?>" name="_web" />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Site web" name="_web" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <?PHP if ($_rss!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_rss; ?>" value="<?PHP echo $_rss; ?>" name="_rss" />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Flux RSS du site web" name="_rss" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <?PHP if ($_facebook!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_facebook; ?>" value="<?PHP echo $_facebook; ?>" name="_facebook" disabled />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Page Facebook" name="_facebook" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                    <div class="control has-icons-left">
                    <?PHP if ($_twitter!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_twitter; ?>" value="<?PHP echo $_twitter; ?>" name="_twitter" />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Profil Twitter" name="_twitter" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                  <label class="label font-bold">Géolocalisation</label>
                  <?PHP if ($_longitude=='') { ?>
                  <p class="help is-dark">Indiquez une localisation aussi précise que possible, idéalement au bâtiment près. N'indiquez pas une géolocalisation sur la ville. Si vous avez un doute, je m'en chargerai :)</p>
                  <?PHP } ?>
                  <div class="control has-icons-left">
                    <?PHP if ($_longitude!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_longitude; ?>" value="<?PHP echo $_longitude; ?>" name="_GPSlon" />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Longitude (ex: -2.345678)" name="_GPSlon" />
                    <?PHP } ?>
                    </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <?PHP if ($_latitude!='') { ?>
                       <input class="input is-warning font-bold" type="text" placeholder="<?PHP echo $_latitude; ?>" value="<?PHP echo $_latitude; ?>" name="_GPSlat" />
                    <?PHP } else { ?>
                       <input class="input" type="text" placeholder="Latitude (ex: 23.456789)" name="_GPSlat" />
                    <?PHP } ?>
                    </div>
                </div>

              </div>

            </div>

          </section>

          <section class="hero is-warning" style="background-color:#4dc9f6;">
            <div class="hero-body">
              <div class="container">
                <h1 class="title">
                  Qui êtes-vous ?
                </h1>

                <div class="field">
                  <?PHP // <label class="label">Nom / Prénom</label> ?>
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Nom / Prénom" name="contact_nom" /></div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Statut" name="contact_statut" /></div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Courriel" name="contact_email" /></div>
                </div>
                <div class="field">
                  <div class="control">
                    <textarea class="textarea" type="text" placeholder="Un message ?" name="contact_message"></textarea>
                  </div>
                </div>

                <input type="hidden" value="<?PHP echo $adresse_ip; ?>" name="adresse_ip" />
                <input type="hidden" value="<?PHP echo $navigateur; ?>" name="navigateur" />
                <input type="hidden" value="<?PHP echo $_GET['id']; ?>" name="identifiant" />

              </div>
            </div>
          </section>

          <section>
          <div class="field" style="margin:30px 0px;">
              <div class="field is-grouped is-grouped-centered">
                <p class="control" id="bouton_envoyer">
                    <button type="submit" class="button is-success font-bold"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="20 6 9 17 4 12"></polyline></svg> &nbsp; <span>Envoyer</span></button>
                <p class="control" id="bouton_annuler">
                    <button type="reset" class="button is-danger font-bold"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="10"></circle><line x1="4.93" y1="4.93" x2="19.07" y2="19.07"></line></svg> &nbsp;<span>Annuler</span></button>
              </div>
            </div>
          </section>

          </FORM>
          <?PHP } else { // Pas de pôle sélectionné => message erreur ?>
          <section class="hero is-danger">
            <div class="hero-body">
              <div class="container">
                <h1 class="title">
                  Sélectionnez un pôle
                </h1>
                <h2 class="subtitle">
                  Pour modifier un pôle, <span class="font-bold"><a href="maplabbzh_liste.php" target="_self">veuillez en sélectionner un</a></span> puis cliquer sur le bouton <i>"Améliorez cette fiche"</i>
                </h2>
              </div>
            </div>
          </section>
          <?PHP } ?>

<?PHP } if ($_GET['cat'] == 'suggerer') { // Suggérer une amélioration ————————————————————————————————————————————————————————————— ?>

          <FORM action="maplabbzh_submit.php?cat=suggerer" method="post">
          <section class="hero is-light">
            <div class="hero-body">
              <div class="container">
                <h1 class="title">
                  Quelle est votre suggestion ?
                </h1>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Objet" name="contact_objet" /></div>
                </div>
                <div class="field">
                  <div class="control">
                    <textarea class="textarea" type="text" placeholder="Votre message" name="contact_message"></textarea>
                  </div>
                </div>

              </div>
            </div>

          </section>

          <section class="hero is-warning" style="background-color:#4dc9f6;">
            <div class="hero-body">
              <div class="container">
                <h1 class="title">
                  Qui êtes-vous ?
                </h1>

                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Nom / Prénom" name="contact_nom" /></div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Statut" name="contact_statut" /></div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Courriel" name="contact_email" /></div>
                </div>

                <input type="hidden" value="<?PHP echo $adresse_ip; ?>" name="adresse_ip" />
                <input type="hidden" value="<?PHP echo $navigateur; ?>" name="navigateur" />
                <input type="hidden" value="<?PHP echo $_GET['id']; ?>" name="identifiant" />

              </div>
            </div>
          </section>

          <section>
          <div class="field" style="margin:30px 0px;">
              <div class="field is-grouped is-grouped-centered">
                <p class="control" id="bouton_envoyer">
                    <button type="submit" class="button is-success font-bold"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="20 6 9 17 4 12"></polyline></svg> &nbsp; <span>Envoyer</span></button>
                <p class="control" id="bouton_annuler">
                    <button type="reset" class="button is-danger font-bold"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="10"></circle><line x1="4.93" y1="4.93" x2="19.07" y2="19.07"></line></svg> &nbsp;<span>Annuler</span></button>
              </div>
            </div>
          </section>
          </FORM>

<?PHP } if ($_GET['cat'] == 'autre') { // Autre ———————————————————————————————————————————————————————————————————————————————————— ?>

          <FORM action="maplabbzh_submit.php?cat=autre" method="post">
          <section class="hero is-light">
            <div class="hero-body">
              <div class="container">
                <h1 class="title">
                  À votre écoute
                </h1>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Objet" name="contact_objet" /></div>
                </div>
                <div class="field">
                  <div class="control">
                    <textarea class="textarea" type="text" placeholder="Votre message" name="contact_message"></textarea>
                  </div>
                </div>

              </div>
            </div>

          </section>

          <section class="hero is-warning" style="background-color:#4dc9f6;">
            <div class="hero-body">
              <div class="container">
                <h1 class="title">
                  Qui êtes-vous ?
                </h1>

                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Nom / Prénom" name="contact_nom" /></div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Statut" name="contact_statut" /></div>
                </div>
                <div class="field">
                  <div class="control has-icons-left">
                    <input class="input" type="text" placeholder="Courriel" name="contact_email" /></div>
                </div>

                <input type="hidden" value="<?PHP echo $adresse_ip; ?>" name="adresse_ip" />
                <input type="hidden" value="<?PHP echo $navigateur; ?>" name="navigateur" />
                <input type="hidden" value="<?PHP echo $_GET['id']; ?>" name="identifiant" />

              </div>
            </div>
          </section>

          <section>
          <div class="field" style="margin:30px 0px;">
              <div class="field is-grouped is-grouped-centered">
                <p class="control" id="bouton_envoyer">
                    <button type="submit" class="button is-success font-bold"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="20 6 9 17 4 12"></polyline></svg> &nbsp; <span>Envoyer</span></button>
                <p class="control" id="bouton_annuler">
                    <button type="reset" class="button is-danger font-bold"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="10"></circle><line x1="4.93" y1="4.93" x2="19.07" y2="19.07"></line></svg> &nbsp;<span>Annuler</span></button>
              </div>
            </div>
          </section>
          </FORM>

          <?PHP } ?>

<?PHP } ?>

<?PHP 	// Menu top
require_once ('maplabbzh_footer.inc.php');
?>

</body>
</html>
